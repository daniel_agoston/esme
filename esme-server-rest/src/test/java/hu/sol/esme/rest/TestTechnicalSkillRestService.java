package hu.sol.esme.rest;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import hu.sol.esme.data.model.Skill;
import hu.sol.esme.data.model.TechnicalSkill;
import hu.sol.esme.data.model.TechnicalSkillGroup;
import hu.sol.esme.rest.controller.SkillRestService;
import io.github.benas.randombeans.EnhancedRandomBuilder;
import io.github.benas.randombeans.api.EnhancedRandom;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-datasource-config.xml",
		"classpath:spring/test-rest-config.xml" })
public class TestTechnicalSkillRestService {

	private static final Logger LOG = LogManager.getLogger(TestTechnicalSkillService.class);

	@Autowired
	private SkillRestService technicalSkillRestService;

	@Test
	public void saveTechnicalSkill() {
		EnhancedRandom enhancedRandom = EnhancedRandomBuilder.aNewEnhancedRandomBuilder().build();
		TechnicalSkill technicalSkill = enhancedRandom.nextObject(TechnicalSkill.class, "id");
		TechnicalSkillGroup technicalSkillGroup = new TechnicalSkillGroup();
		technicalSkillGroup.setId(new Long(1));
		technicalSkillGroup.setName("Lol");
		technicalSkillGroup.setDescription("asdasdas");
		technicalSkill.setTechnicalSkillGroup(technicalSkillGroup);
		technicalSkillGroup.getTechnicalSkills().add(technicalSkill);

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getForObject("", Skill.class);
		technicalSkillRestService.saveTechnicalSkill(technicalSkill);
		Assert.assertNotNull("Result expected", technicalSkill);
	}
}
