package hu.sol.esme.rest;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import hu.sol.esme.rest.controller.impl.SkillGroupRestServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-datasource-config.xml",
		"classpath:spring/test-rest-config.xml" })
public class TestTechnicalSkillGroupRestService {

	private static final Logger LOG = LogManager.getLogger(TestTechnicalSkillService.class);

	@Autowired
	private SkillGroupRestServiceImpl SkillGroupRestService;
}
