package hu.sol.esme.rest.controller.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import hu.sol.esme.data.model.AspectOpinion;
import hu.sol.esme.data.model.SkillOpinion;
import hu.sol.esme.rest.controller.AbstractRestService;
import hu.sol.esme.rest.controller.OpinionRestService;
import hu.sol.esme.rest.type.ResponseMessageEnum;
import hu.sol.esme.rest.type.RestResponseBody;
import hu.sol.esme.service.transaction.OpinionService;

@RestController
@RequestMapping("/rest/v1/opinion")
public class OpinionRestServiceImpl extends AbstractRestService implements OpinionRestService {

	private static final Logger LOG = LogManager.getLogger(OpinionRestServiceImpl.class);

	@Autowired
	private OpinionService opinionService;

	@Override
	@RequestMapping(value = "/skill/{id}", method = RequestMethod.GET, produces = { "application/json",
			"application/xml" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<SkillOpinion> getSkillOpinionById(@PathVariable("id") Long id) {
		LOG.debug("getSkillOpinionById called.");

		RestResponseBody<SkillOpinion> result = new RestResponseBody<SkillOpinion>();

		SkillOpinion response = opinionService.getSkillOpinionById(id);

		if (response == null) {
			LOG.error("SkillOpinion is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/skill/{username}/user/creator", method = RequestMethod.GET, produces = {
			"application/json", "application/xml" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<SkillOpinion> listSkillOpinionByCreator(
			@PathVariable("username") String usernname) {
		LOG.debug("listSkillOpinionByCreator called.");

		RestResponseBody<SkillOpinion> result = new RestResponseBody<SkillOpinion>();

		List<SkillOpinion> response = opinionService.listSkillOpinionByCreator(usernname);

		if (CollectionUtils.isEmpty(response)) {
			LOG.error("SkillOpinion is empty.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().addAll(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/skill/{username}/user/about", method = RequestMethod.GET, produces = { "application/json",
			"application/xml" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<SkillOpinion> listSkillOpinionByName(@PathVariable("username") String name) {
		LOG.debug("listSkillOpinionByName called.");

		RestResponseBody<SkillOpinion> result = new RestResponseBody<SkillOpinion>();

		List<SkillOpinion> response = opinionService.listSkillOpinionByUsername(name);

		if (CollectionUtils.isEmpty(response)) {
			LOG.error("SkillOpinion is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().addAll(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/skill/create", method = RequestMethod.POST, produces = { "application/json",
			"application/xml" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<SkillOpinion> saveSkillOpinion(@RequestBody SkillOpinion opinion) {
		LOG.debug("saveSkillOpinion called.");

		RestResponseBody<SkillOpinion> result = new RestResponseBody<SkillOpinion>();

		SkillOpinion response = opinionService.saveSkillOpinion(opinion);

		if (response == null) {
			LOG.error("SkillOpinion is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/skill/update", method = RequestMethod.PUT, produces = { "application/json",
			"application/xml" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<SkillOpinion> updateSkillOpinion(@RequestBody SkillOpinion opinion) {
		LOG.debug("updateSkillOpinion called.");

		RestResponseBody<SkillOpinion> result = new RestResponseBody<SkillOpinion>();

		SkillOpinion response = opinionService.updateSkillOpinion(opinion);

		if (response == null) {
			LOG.error("SkillOpinion is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/skill/{id}", method = RequestMethod.DELETE, produces = { "application/json",
			"application/xml" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<SkillOpinion> deleteSkillOpinion(@PathVariable("id") Long id) {
		LOG.debug("deleteSkillOpinion called.");

		RestResponseBody<SkillOpinion> result = new RestResponseBody<SkillOpinion>();

		opinionService.deleteSkillOpinionById(id);

		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/aspect/{id}", method = RequestMethod.GET, produces = { "application/json",
			"application/xml" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<AspectOpinion> getAspectOpinionById(@PathVariable("id") Long id) {
		LOG.debug("getAspectOpinionById called.");

		RestResponseBody<AspectOpinion> result = new RestResponseBody<AspectOpinion>();

		AspectOpinion response = opinionService.getAspectOpinionById(id);

		if (response == null) {
			LOG.error("AspectOpinion is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/aspect/{username}/user/creator", method = RequestMethod.GET, produces = {
			"application/json", "application/xml" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<AspectOpinion> listAspectOpinionByCreator(
			@PathVariable("username") String username) {
		LOG.debug("listAspectOpinionByCreator called.");

		RestResponseBody<AspectOpinion> result = new RestResponseBody<AspectOpinion>();

		List<AspectOpinion> response = opinionService.listAspectOpinionByCreator(username);

		if (CollectionUtils.isEmpty(response)) {
			LOG.error("AspectOpinion is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().addAll(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/aspect/{username}/user/about", method = RequestMethod.GET, produces = {
			"application/json", "application/xml" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<AspectOpinion> listAspectOpinionByName(
			@PathVariable("username") String username) {
		LOG.debug("listAspectOpinionByName called.");

		RestResponseBody<AspectOpinion> result = new RestResponseBody<AspectOpinion>();

		List<AspectOpinion> response = opinionService.listAspectOpinionByUsername(username);

		if (CollectionUtils.isEmpty(response)) {
			LOG.error("AspectOpinion is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().addAll(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/aspect/create", method = RequestMethod.POST, produces = { "application/json",
			"application/xml" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<AspectOpinion> saveAspectOpinion(@RequestBody AspectOpinion opinion) {
		LOG.debug("saveSkillOpinion called.");

		RestResponseBody<AspectOpinion> result = new RestResponseBody<AspectOpinion>();

		AspectOpinion response = opinionService.saveAspectOpinion(opinion);

		if (response == null) {
			LOG.error("AspectOpinion is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/aspect/update", method = RequestMethod.PUT, produces = { "application/json",
			"application/xml" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<AspectOpinion> updateAspectOpinion(@RequestBody AspectOpinion opinion) {
		LOG.debug("saveSkillOpinion called.");

		RestResponseBody<AspectOpinion> result = new RestResponseBody<AspectOpinion>();

		AspectOpinion response = opinionService.updateAspectOpinion(opinion);

		if (response == null) {
			LOG.error("AspectOpinion is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/aspect/{id}", method = RequestMethod.DELETE, produces = { "application/json",
			"application/xml" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<AspectOpinion> deleteAspectOpinion(@PathVariable("id") Long id) {
		LOG.debug("deleteSkillOpinion called.");

		RestResponseBody<AspectOpinion> result = new RestResponseBody<AspectOpinion>();

		opinionService.deleteSkillOpinionById(id);

		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

}
