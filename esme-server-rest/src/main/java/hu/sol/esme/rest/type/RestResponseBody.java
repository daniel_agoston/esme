package hu.sol.esme.rest.type;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RestResponseBody<T> {

	@JsonProperty
	String msg;

	@JsonProperty
	String code;

	@JsonProperty
	List<T> result;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<T> getResult() {
		if (result == null) {
			return result = new ArrayList<T>();
		}
		return result;
	}

	public void setResult(List<T> result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "RestResponseBody [msg=" + msg + ", code=" + code + ", result=" + result + "]";
	}

}
