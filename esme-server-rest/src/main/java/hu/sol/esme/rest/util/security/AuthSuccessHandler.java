package hu.sol.esme.rest.util.security;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import hu.sol.esme.data.model.User;
import hu.sol.esme.rest.util.json.hibernate.HibernateAwareObjectMapper;

@Component(value = "restSuccessHandler")
public class AuthSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
	private static final Logger LOG = LogManager.getLogger(AuthSuccessHandler.class);

	@Autowired
	private HibernateAwareObjectMapper mapper;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		response.setStatus(HttpServletResponse.SC_OK);

		User user = (User) authentication.getPrincipal();

		LOG.debug(user.getUsername() + " got is connected ");

		PrintWriter writer = response.getWriter();
		mapper.writeValue(writer, user);
		writer.flush();
	}
}
