package hu.sol.esme.rest.controller;

import hu.sol.esme.data.model.Aspect;
import hu.sol.esme.rest.type.RestResponseBody;

public interface AspectRestService {

	public RestResponseBody<Aspect> getAspectById(Long id);

	public RestResponseBody<Aspect> listAspects();

	public RestResponseBody<Aspect> listAspectsByName(String name);

	public RestResponseBody<Aspect> saveAspect(Aspect aspect);

	public RestResponseBody<Aspect> updateAspect(Aspect aspect);

	public RestResponseBody<Aspect> deleteAspect(Long id);

}
