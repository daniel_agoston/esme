/**
 * The MIT License (MIT)

Copyright (c) 2014 Jack Matthews

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package hu.sol.esme.rest.util.json.response;

import java.io.IOException;
import java.lang.reflect.Type;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;

import hu.sol.esme.rest.util.json.response.annotation.JsonMixin;

/**
 * Proccessing JsonMixin-s
 * 
 */
final class JsonResponseAwareJsonMessageConverter extends MappingJackson2HttpMessageConverter {

	private static final Logger LOG = LogManager.getLogger(JsonResponseAwareJsonMessageConverter.class);

	@Override
	protected void writeInternal(Object object, Type type, HttpOutputMessage outputMessage)
			throws IOException, HttpMessageNotWritableException {

		if (object instanceof ResponseWrapper) {
			writeJson((ResponseWrapper) object, outputMessage);
		} else {
			super.writeInternal(object, type, outputMessage);
		}
	}

	/**
	 * Writing json from mixins
	 * 
	 * @param response
	 * @param outputMessage
	 * @throws IOException
	 * @throws HttpMessageNotWritableException
	 */
	protected void writeJson(ResponseWrapper response, HttpOutputMessage outputMessage)
			throws IOException, HttpMessageNotWritableException {

		JsonEncoding encoding = getJsonEncoding(outputMessage.getHeaders().getContentType());

		JsonMixin[] jsonMixins = response.getJsonResponse().mixins();

		for (JsonMixin jsonMixin : jsonMixins) {
			objectMapper.addMixIn(jsonMixin.target(), jsonMixin.mixin());
		}

		JsonGenerator jsonGenerator = objectMapper.getFactory().createGenerator(outputMessage.getBody(), encoding);
		try {
			objectMapper.writeValue(jsonGenerator, response.getOriginalResponse());
		} catch (IOException ex) {
			throw new HttpMessageNotWritableException("Could not write JSON: " + ex.getMessage(), ex);
		}
	}

}
