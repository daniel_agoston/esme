package hu.sol.esme.rest.util.security;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.web.bind.annotation.RestController;

import hu.sol.esme.rest.type.ResponseMessageEnum;
import hu.sol.esme.rest.type.RestResponseBody;
import hu.sol.esme.rest.util.json.hibernate.HibernateAwareObjectMapper;

@RestController(value = "restAuthenticationEntryPoint")
public class HttpAuthenticationEntryPoint implements AuthenticationEntryPoint {

	private static final Logger LOG = LogManager.getLogger(HttpAuthenticationEntryPoint.class);

	@Autowired
	private HibernateAwareObjectMapper objectMapper;

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException {
		LOG.debug("commence called.");
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

		RestResponseBody<String> error = new RestResponseBody<String>();
		error.setMsg(ResponseMessageEnum.LOGIN_FAILED.getMessage());
		error.setCode(String.valueOf(ResponseMessageEnum.LOGIN_FAILED.getCode()));
		error.getResult().add(authException.getMessage());

		PrintWriter writer = response.getWriter();
		writer.write(objectMapper.writeValueAsString(error));
		writer.flush();
	}
}
