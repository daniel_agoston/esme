package hu.sol.esme.rest.controller;

import org.springframework.web.bind.annotation.RequestBody;

import hu.sol.esme.data.model.Room;
import hu.sol.esme.rest.type.RestResponseBody;

public interface RoomRestService {

	public RestResponseBody<Room> listRoomByUserName(String username);

	public RestResponseBody<Room> listRoomByUserEmail(String email);

	public RestResponseBody<Room> listRoomByUserId(Long userId);

	public RestResponseBody<Room> listRoom();

	public RestResponseBody<Room> getRoomById(Long roomId);

	public RestResponseBody<Room> createRoom(Room room);

	public RestResponseBody<Room> updateRoom(Room room);

	public RestResponseBody<Room> setRoomOpeningTime(Room room);

}
