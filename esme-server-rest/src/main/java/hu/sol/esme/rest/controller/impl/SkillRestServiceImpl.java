package hu.sol.esme.rest.controller.impl;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.thoughtworks.xstream.XStream;

import hu.sol.esme.data.model.IndustrySkill;
import hu.sol.esme.data.model.TechnicalSkill;
import hu.sol.esme.rest.controller.AbstractRestService;
import hu.sol.esme.rest.controller.SkillRestService;
import hu.sol.esme.rest.type.ResponseMessageEnum;
import hu.sol.esme.rest.type.RestResponseBody;
import hu.sol.esme.service.transaction.SkillService;

@RestController
@RequestMapping("/rest/v1/skill")
public class SkillRestServiceImpl extends AbstractRestService implements SkillRestService {

	private static final Logger LOG = LogManager.getLogger(SkillRestServiceImpl.class);

	@Autowired
	private SkillService skillService;

	@PostConstruct
	private void initSkillRest() {
		LOG.debug("init SkillRest Service");
	}

	//////////////////////////////
	// Technical skill services
	//////////////////////////////

	@Override
	@RequestMapping(value = "/technical/{id}", method = RequestMethod.GET, produces = {
			"application/json" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<TechnicalSkill> getTechnicalSkillById(@PathVariable("id") Long id) {
		LOG.debug("getTechnicalSkillById called.");

		RestResponseBody<TechnicalSkill> result = new RestResponseBody<TechnicalSkill>();

		TechnicalSkill response = skillService.getTechnicalSkillById(id);

		if (response == null) {
			LOG.error("TechnicalSkill is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/technical/create", method = RequestMethod.POST, produces = {
			"application/json" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<TechnicalSkill> saveTechnicalSkill(
			@RequestBody TechnicalSkill technicalSkill) {
		LOG.debug("saveTechnicalSkill called.");

		RestResponseBody<TechnicalSkill> result = new RestResponseBody<TechnicalSkill>();

		TechnicalSkill response = skillService.saveTechnicalSkill(technicalSkill);

		if (response == null) {
			LOG.error("TechnicalSkill is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}
		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		LOG.debug(new XStream().toXML(result));
		return result;
	}

	@Override
	@RequestMapping(value = "/technical/{id}/withgroup", method = RequestMethod.GET, produces = {
			"application/json" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<TechnicalSkill> getTechnicalSkillByIdWithGroup(@PathVariable("id") Long id)
			throws Exception {
		LOG.debug("getTechnicalSkillByIdWithGroup called.");

		RestResponseBody<TechnicalSkill> result = new RestResponseBody<TechnicalSkill>();

		TechnicalSkill response = skillService.getTechnicalSkillByIdWithGroup(id);

		if (response == null) {
			LOG.error("TechnicalSkill is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}
		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		LOG.debug(new XStream().toXML(result));
		return result;

	}

	@Override
	@RequestMapping(value = "/technical/{id}/withdepth", method = RequestMethod.GET, produces = {
			"application/json" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<TechnicalSkill> getTechnicalSkillByIdWithDepth(@PathVariable("id") Long id) {
		LOG.debug("getTechnicalSkillByIdWithDepth called.");

		RestResponseBody<TechnicalSkill> result = new RestResponseBody<TechnicalSkill>();

		TechnicalSkill response = skillService.getTechnicalSkillByIdWithGroup(id);

		if (response == null) {
			LOG.error("TechnicalSkill is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}
		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	//////////////////////////////
	// Industry skill services
	//////////////////////////////

	@Override
	@RequestMapping(value = "/industry/create", method = RequestMethod.POST, produces = {
			"application/json" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<IndustrySkill> saveIndustrySkill(@RequestBody IndustrySkill industrySkill) {
		LOG.debug("getIndustrySkillById called.");

		RestResponseBody<IndustrySkill> result = new RestResponseBody<IndustrySkill>();

		IndustrySkill response = skillService.saveIndustrySkill(industrySkill);

		if (response == null) {
			LOG.error("IndustrySkill is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/industry/{id}", method = RequestMethod.GET, produces = {
			"application/json" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<IndustrySkill> getIndustrySkilllById(@PathVariable("id") Long id) {
		LOG.debug("getIndustrySkillById called.");

		RestResponseBody<IndustrySkill> result = new RestResponseBody<IndustrySkill>();

		IndustrySkill response = skillService.getIndustrySkillById(id);

		if (response == null) {
			LOG.error("IndustrySkill is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/industry/{id}/withgroup", method = RequestMethod.GET, produces = {
			"application/json" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<IndustrySkill> getIndustrySkillByIdWithGroup(@PathVariable("id") Long id) {
		LOG.debug("getIndustrySkillById called.");
		LOG.debug("HANSOLO: " + id);

		RestResponseBody<IndustrySkill> result = new RestResponseBody<IndustrySkill>();

		IndustrySkill response = skillService.getIndustrySkillByIdWithGroup(id);

		if (response == null) {
			LOG.error("IndustrySkill is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/industry/{id}/withdepth", method = RequestMethod.GET, produces = {
			"application/json" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<IndustrySkill> getIndustrySkillByIdWithDepth(@PathVariable("id") Long id) {
		LOG.debug("getIndustrySkillByIdWithDepth called.");

		RestResponseBody<IndustrySkill> result = new RestResponseBody<IndustrySkill>();

		IndustrySkill response = skillService.getIndustrySkillByIdWithGroup(id);

		if (response == null) {
			LOG.error("IndustrySkill is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}
		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

}
