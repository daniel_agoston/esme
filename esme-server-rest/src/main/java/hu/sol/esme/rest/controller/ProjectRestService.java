package hu.sol.esme.rest.controller;

import hu.sol.esme.data.model.Project;
import hu.sol.esme.rest.type.RestResponseBody;

public interface ProjectRestService {

	public RestResponseBody<Project> getProjectById(Long id);

	public RestResponseBody<Project> listProjects();

	public RestResponseBody<Project> listProjectsByName(String name);

	public RestResponseBody<Project> saveProject(Project project);

	public RestResponseBody<Project> updateProject(Project project);

	public RestResponseBody<Project> deleteProject(Long id);
}
