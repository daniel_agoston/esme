package hu.sol.esme.rest.mixin;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import hu.sol.esme.data.model.Depth;
import hu.sol.esme.data.model.Skill;

public abstract class SkillDepthWithSkill {

	@JsonProperty
	@JsonBackReference("skillDepth")
	abstract Skill getSkill();

	@JsonProperty
	abstract Depth getDepth();
}
