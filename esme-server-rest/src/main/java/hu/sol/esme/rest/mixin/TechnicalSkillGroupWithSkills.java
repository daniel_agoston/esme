package hu.sol.esme.rest.mixin;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import hu.sol.esme.data.model.TechnicalSkill;

public abstract class TechnicalSkillGroupWithSkills {

	@JsonProperty
	abstract String getId();

	@JsonProperty
	abstract String getName();

	@JsonProperty
	abstract String getDescription();

	@JsonProperty
	@JsonManagedReference("technicalSkills")
	abstract Set<TechnicalSkill> getTechnicalSkills();
}
