package hu.sol.esme.rest.mixin;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import hu.sol.esme.data.model.IndustrySkill;

public abstract class IndustrySkillGroupWithSkills {

	@JsonProperty
	abstract String getId();

	@JsonProperty
	abstract String getName();

	@JsonProperty
	abstract String getDescription();

	@JsonProperty
	@JsonManagedReference("industrySkills")
	abstract Set<IndustrySkill> getIndustrySkills();
}