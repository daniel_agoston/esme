package hu.sol.esme.rest.type;

public enum ResponseMessageEnum {
	// @formatter:off
	SUCCESS(200,"SUCCESS"),
	ERROR(400,"ERROR"),
	LOGIN_FAILED(401,"AUTHENTICATION FAILED"),
	FAILED(403,"FAILED"),
	EMPTY(404,"EMPTY");
	// @formatter:on

	private final int code;
	private final String message;

	ResponseMessageEnum(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return this.code;
	}

	public String getMessage() {
		return this.message;
	}
}
