package hu.sol.esme.rest.controller.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import hu.sol.esme.data.model.Aspect;
import hu.sol.esme.rest.controller.AbstractRestService;
import hu.sol.esme.rest.controller.AspectRestService;
import hu.sol.esme.rest.type.ResponseMessageEnum;
import hu.sol.esme.rest.type.RestResponseBody;
import hu.sol.esme.service.transaction.AspectService;

@RestController
@RequestMapping("/rest/v1/aspect")
public class AspectRestServiceImpl extends AbstractRestService implements AspectRestService {

	private static final Logger LOG = LogManager.getLogger(OpinionRestServiceImpl.class);

	@Autowired
	private AspectService aspectService;

	@Override
	@RequestMapping(value = "/aspect/{id}", method = RequestMethod.GET, produces = { "application/json",
			"application/xml" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<Aspect> getAspectById(@PathVariable("id") Long id) {
		LOG.debug("getAspectById called.");

		RestResponseBody<Aspect> result = new RestResponseBody<Aspect>();

		Aspect response = aspectService.getAspectById(id);

		if (response == null) {
			LOG.error("Aspect is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/aspect/all", method = RequestMethod.GET, produces = { "application/json",
			"application/xml" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<Aspect> listAspects() {
		LOG.debug("listAspects called.");

		RestResponseBody<Aspect> result = new RestResponseBody<Aspect>();

		List<Aspect> response = aspectService.listAspect();

		if (CollectionUtils.isEmpty(response)) {
			LOG.error("Aspect is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().addAll(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/aspect/{name}/name", method = RequestMethod.GET, produces = { "application/json",
			"application/xml" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<Aspect> listAspectsByName(@PathVariable("name") String name) {
		LOG.debug("listAspectsByName called.");

		RestResponseBody<Aspect> result = new RestResponseBody<Aspect>();

		List<Aspect> response = aspectService.listAspectByName(name);

		if (CollectionUtils.isEmpty(response)) {
			LOG.error("Aspect is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().addAll(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/aspect/create", method = RequestMethod.POST, produces = { "application/json",
			"application/xml" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<Aspect> saveAspect(@RequestBody Aspect aspect) {
		LOG.debug("saveAspect called.");

		RestResponseBody<Aspect> result = new RestResponseBody<Aspect>();

		Aspect response = aspectService.saveAspect(aspect);

		if (response == null) {
			LOG.error("Aspect is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/aspect/update", method = RequestMethod.PUT, produces = { "application/json",
			"application/xml" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<Aspect> updateAspect(@RequestBody Aspect aspect) {
		LOG.debug("getAspectById called.");

		RestResponseBody<Aspect> result = new RestResponseBody<Aspect>();

		Aspect response = aspectService.updateAspect(aspect);

		if (response == null) {
			LOG.error("Aspect is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/aspect/{id}", method = RequestMethod.DELETE, produces = { "application/json",
			"application/xml" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<Aspect> deleteAspect(@PathVariable("id") Long id) {
		LOG.debug("deleteSkillOpinion called.");

		RestResponseBody<Aspect> result = new RestResponseBody<Aspect>();

		aspectService.deleteAspectById(id);

		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

}
