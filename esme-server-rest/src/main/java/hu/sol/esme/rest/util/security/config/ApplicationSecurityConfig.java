package hu.sol.esme.rest.util.security.config;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import hu.sol.esme.rest.util.security.AuthFailureHandler;
import hu.sol.esme.rest.util.security.AuthSuccessHandler;
import hu.sol.esme.rest.util.security.HttpAuthenticationEntryPoint;

@Configuration
@EnableWebSecurity
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

	private static final Logger LOG = LogManager.getLogger(ApplicationSecurityConfig.class);

	@Resource(name = "esmeUserDetailsService")
	private UserDetailsService userDetailsService;

	@Resource(name = "restAuthenticationEntryPoint")
	private HttpAuthenticationEntryPoint authenticationEntryPoint;

	@Resource(name = "restFailureHandler")
	private AuthFailureHandler authenticationFailureHandler;

	@Resource(name = "restSuccessHandler")
	private AuthSuccessHandler authenticationSuccessHandler;

	@Override
	protected void configure(AuthenticationManagerBuilder builder) throws Exception {
		builder.userDetailsService(userDetailsService).passwordEncoder(new BCryptPasswordEncoder());
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/resources/**"); // #3
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		//@formatter:off
		
		http.httpBasic()
			.and().authorizeRequests().antMatchers("/rest/**").hasRole("USER").anyRequest().anonymous()
			.and().exceptionHandling().authenticationEntryPoint(authenticationEntryPoint)
			.and().formLogin().successHandler(authenticationSuccessHandler)
			.and().formLogin().failureHandler(authenticationFailureHandler)
			.and().logout().logoutSuccessUrl("/")
			.and().csrf().disable();
//		http.authorizeRequests().antMatchers("/**").permitAll();

		//@formatter:on

		// CSRF tokens handling
		// http.addFilterAfter(new CsrfTokenResponseHeaderBindingFilter(),
		// CsrfFilter.class);
	}

}
