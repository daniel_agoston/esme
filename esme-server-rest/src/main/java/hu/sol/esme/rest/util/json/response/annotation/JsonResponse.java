/**
 * The MIT License (MIT)

Copyright (c) 2014 Jack Matthews

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package hu.sol.esme.rest.util.json.response.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import hu.sol.esme.rest.util.json.response.JsonResponseInjectingReturnValueHandler;

/**
 * Use mixin for different jsonreponse..
 * 
 * @see JsonResponseInjectingReturnValueHandler#handleReturnValue(Object,
 *      org.springframework.core.MethodParameter,
 *      org.springframework.web.method.support.ModelAndViewContainer,
 *      org.springframework.web.context.request.NativeWebRequest)
 * 
 * @version 1.0.0
 * @since 1.0.0
 * @author Daniel Agoston
 *
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value = { ElementType.METHOD })
public @interface JsonResponse {

	public JsonMixin[] mixins();

}
