package hu.sol.esme.rest.controller;

import hu.sol.esme.data.model.AspectOpinion;
import hu.sol.esme.data.model.SkillOpinion;
import hu.sol.esme.rest.type.RestResponseBody;

public interface OpinionRestService {

	public RestResponseBody<SkillOpinion> getSkillOpinionById(Long id);

	public RestResponseBody<SkillOpinion> listSkillOpinionByCreator(String username);

	public RestResponseBody<SkillOpinion> listSkillOpinionByName(String username);

	public RestResponseBody<SkillOpinion> saveSkillOpinion(SkillOpinion opinion);

	public RestResponseBody<SkillOpinion> updateSkillOpinion(SkillOpinion opinion);

	public RestResponseBody<SkillOpinion> deleteSkillOpinion(Long id);

	public RestResponseBody<AspectOpinion> getAspectOpinionById(Long id);

	public RestResponseBody<AspectOpinion> listAspectOpinionByCreator(String name);

	public RestResponseBody<AspectOpinion> listAspectOpinionByName(String name);

	public RestResponseBody<AspectOpinion> saveAspectOpinion(AspectOpinion opinion);

	public RestResponseBody<AspectOpinion> updateAspectOpinion(AspectOpinion opinion);

	public RestResponseBody<AspectOpinion> deleteAspectOpinion(Long id);
}
