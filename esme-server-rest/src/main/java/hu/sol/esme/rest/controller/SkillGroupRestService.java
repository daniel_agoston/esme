package hu.sol.esme.rest.controller;

import hu.sol.esme.data.model.IndustrySkillGroup;
import hu.sol.esme.data.model.TechnicalSkillGroup;
import hu.sol.esme.rest.type.RestResponseBody;

public interface SkillGroupRestService {

	public RestResponseBody<TechnicalSkillGroup> getTechnicalSkillGroupByIdWithSkills(Long id);

	public RestResponseBody<TechnicalSkillGroup> getTechnicalSkillGroupById(Long id);

	public RestResponseBody<TechnicalSkillGroup> listTechnicalSkillGroup();

	public RestResponseBody<IndustrySkillGroup> getIndustrySkillGroupByIdWithSkills(Long id);

	public RestResponseBody<IndustrySkillGroup> getIndustrySkillGroupById(Long id);

	public RestResponseBody<IndustrySkillGroup> listIndustrySkillGroup();
}
