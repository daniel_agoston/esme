package hu.sol.esme.rest.mixin;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import hu.sol.esme.data.model.SkillDepth;
import hu.sol.esme.data.model.TechnicalSkillGroup;

public abstract class TechnicalSkillWithDepth {
	
	@JsonProperty
	abstract Long getId();

	@JsonProperty
	abstract String getName();

	@JsonProperty
	abstract String getDescription();

	@JsonIgnore
	@JsonManagedReference("skillDepth")
	abstract SkillDepth getSkillDepth();

	@JsonIgnore
	@JsonBackReference("technicalSkills")
	abstract TechnicalSkillGroup getTechnicalSkillGroup();
}
