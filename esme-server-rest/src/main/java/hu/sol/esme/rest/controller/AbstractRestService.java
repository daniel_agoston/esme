package hu.sol.esme.rest.controller;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import hu.sol.esme.rest.type.ResponseMessageEnum;
import hu.sol.esme.rest.type.RestResponseBody;

public abstract class AbstractRestService {

	protected <T> void setResponseMessage(RestResponseBody<T> response, ResponseMessageEnum message) {
		response.setMsg(message.getMessage());
		response.setCode(String.valueOf(message.getCode()));
	}

	@ExceptionHandler(Exception.class)
	@ResponseBody
	@ResponseStatus(value = HttpStatus.CONFLICT)
	protected RestResponseBody<String> exceptionHandler(Exception ex) {
		RestResponseBody<String> error = new RestResponseBody<String>();
		setResponseMessage(error, ResponseMessageEnum.ERROR);
		error.getResult().add(ex.getMessage());
		return error;
	}

	@ExceptionHandler(UsernameNotFoundException.class)
	@ResponseBody
	@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
	protected RestResponseBody<String> userNotFoundExceptionHandler(UsernameNotFoundException ex) {
		RestResponseBody<String> error = new RestResponseBody<String>();
		setResponseMessage(error, ResponseMessageEnum.LOGIN_FAILED);
		error.getResult().add(ex.getMessage());
		return error;
	}
}
