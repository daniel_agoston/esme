package hu.sol.esme.rest.mixin;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import hu.sol.esme.data.model.IndustrySkillGroup;
import hu.sol.esme.data.model.SkillDepth;

public abstract class IndustrySkillWithoutGroup {

	@JsonProperty
	abstract Long getId();

	@JsonProperty
	abstract String getName();

	@JsonProperty
	abstract String getDescription();

	@JsonProperty
	abstract SkillDepth getSkillDepth();

	@JsonIgnore
	@JsonBackReference("industrySkills")
	abstract IndustrySkillGroup getIndustrySkillGroup();
}
