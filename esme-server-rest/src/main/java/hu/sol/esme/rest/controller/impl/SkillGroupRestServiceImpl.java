package hu.sol.esme.rest.controller.impl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import hu.sol.esme.data.model.IndustrySkill;
import hu.sol.esme.data.model.IndustrySkillGroup;
import hu.sol.esme.data.model.TechnicalSkill;
import hu.sol.esme.data.model.TechnicalSkillGroup;
import hu.sol.esme.rest.controller.AbstractRestService;
import hu.sol.esme.rest.controller.SkillGroupRestService;
import hu.sol.esme.rest.mixin.IndustrySkillGroupWithSkills;
import hu.sol.esme.rest.mixin.IndustrySkillWithoutGroupDepth;
import hu.sol.esme.rest.mixin.TechnicalSkillGroupWithSkills;
import hu.sol.esme.rest.mixin.TechnicalSkillWithoutGroupDepth;
import hu.sol.esme.rest.type.ResponseMessageEnum;
import hu.sol.esme.rest.type.RestResponseBody;
import hu.sol.esme.rest.util.json.response.annotation.JsonMixin;
import hu.sol.esme.rest.util.json.response.annotation.JsonResponse;
import hu.sol.esme.service.transaction.SkillGroupService;

@RestController
@RequestMapping("/rest/v1/skillgroup")
public class SkillGroupRestServiceImpl extends AbstractRestService implements SkillGroupRestService {

	private static final Logger LOG = LogManager.getLogger(SkillGroupRestServiceImpl.class);

	@Autowired
	private SkillGroupService skillGroupService;

	@PostConstruct
	private void initSkillRest() {
		LOG.debug("init SkillRest Service");
	}

	@Override
	@RequestMapping(value = "/technical/{id}/withskills", method = RequestMethod.GET, produces = {
			"application/json; charset=UTF-8" }, headers = "Accept=*")
	@JsonResponse(mixins = {
			@JsonMixin(mixin = TechnicalSkillGroupWithSkills.class, target = TechnicalSkillGroup.class),
			@JsonMixin(mixin = TechnicalSkillWithoutGroupDepth.class, target = TechnicalSkill.class) })
	public @ResponseBody RestResponseBody<TechnicalSkillGroup> getTechnicalSkillGroupByIdWithSkills(
			@PathVariable("id") Long id) {
		LOG.debug("getTechnicalSkillGroupByIdWithSkills called.");

		RestResponseBody<TechnicalSkillGroup> result = new RestResponseBody<TechnicalSkillGroup>();

		LOG.debug("Debug point 1");
		TechnicalSkillGroup response = skillGroupService.getTechnicalSkillGroupByIdWithSkills(id);
		LOG.debug("Debug point 2");

		if (response == null) {
			LOG.error("TechnicalSkillGroup is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/technical/{id}", method = RequestMethod.GET, produces = {
			"application/json" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<TechnicalSkillGroup> getTechnicalSkillGroupById(@PathVariable("id") Long id) {
		LOG.debug("getTechnicalSkillGroupById called.");

		RestResponseBody<TechnicalSkillGroup> result = new RestResponseBody<TechnicalSkillGroup>();

		LOG.debug("Debug point 1");
		TechnicalSkillGroup response = skillGroupService.getTechnicalSkillGroupById(id);
		LOG.debug("Debug point 2");

		if (response == null) {
			LOG.error("TechnicalSkillGroup is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/technical/all", method = RequestMethod.GET, produces = {
			"application/json" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<TechnicalSkillGroup> listTechnicalSkillGroup() {
		LOG.debug("listTechnicalSkillGroup called.");

		RestResponseBody<TechnicalSkillGroup> result = new RestResponseBody<TechnicalSkillGroup>();

		LOG.debug("Debug point 1");
		List<TechnicalSkillGroup> response = skillGroupService.listTechnicalSkillGroup();
		LOG.debug("Debug point 2");

		if (CollectionUtils.isEmpty(response)) {
			LOG.error("TechnicalSkillGroup is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().addAll(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	/////////////////////////////////
	// Industry skill group services
	/////////////////////////////////

	@Override
	@RequestMapping(value = "/industry/{id}/withskills", method = RequestMethod.GET, produces = {
			"application/json" }, headers = "Accept=*")
	@JsonResponse(mixins = { @JsonMixin(mixin = IndustrySkillGroupWithSkills.class, target = IndustrySkillGroup.class),
			@JsonMixin(mixin = IndustrySkillWithoutGroupDepth.class, target = IndustrySkill.class) })
	public @ResponseBody RestResponseBody<IndustrySkillGroup> getIndustrySkillGroupByIdWithSkills(Long id) {

		RestResponseBody<IndustrySkillGroup> result = new RestResponseBody<IndustrySkillGroup>();

		LOG.debug("Debug point 1");
		IndustrySkillGroup response = skillGroupService.getIndustrySkillGroupByIdWithSkills(id);
		LOG.debug("Debug point 2");

		if (response == null) {
			LOG.error("IndustrySkillGroup is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/industry/{id}", method = RequestMethod.GET, produces = {
			"application/json" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<IndustrySkillGroup> getIndustrySkillGroupById(Long id) {
		LOG.debug("getIndustrySkillGroupGroupById called.");

		RestResponseBody<IndustrySkillGroup> result = new RestResponseBody<IndustrySkillGroup>();

		LOG.debug("Debug point 1");
		IndustrySkillGroup response = skillGroupService.getIndustrySkillGroupById(id);
		LOG.debug("Debug point 2");

		if (response == null) {
			LOG.error("IndustrySkillGroup is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/industry/all", method = RequestMethod.GET, produces = {
			"application/json" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<IndustrySkillGroup> listIndustrySkillGroup() {
		LOG.debug("listIndustrySkillGroup called.");

		RestResponseBody<IndustrySkillGroup> result = new RestResponseBody<IndustrySkillGroup>();

		LOG.debug("Debug point 1");
		List<IndustrySkillGroup> response = skillGroupService.listIndustrySkillGroup();
		LOG.debug("Debug point 2");

		if (CollectionUtils.isEmpty(response)) {
			LOG.error("IndustrySkillGroup is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().addAll(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}
}
