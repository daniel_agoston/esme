package hu.sol.esme.rest.mixin;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import hu.sol.esme.data.model.SkillDepth;
import hu.sol.esme.data.model.TechnicalSkillGroup;

public abstract class TechnicalSkillWithoutGroup {

	@JsonProperty
	abstract Long getId();

	@JsonProperty
	abstract String getName();

	@JsonProperty
	abstract String getDescription();

	@JsonProperty
	abstract SkillDepth getSkillDepth();

	@JsonIgnore
	@JsonBackReference("technicalSkills")
	abstract TechnicalSkillGroup getTechnicalSkillGroup();

}
