package hu.sol.esme.rest.controller.impl;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hu.sol.esme.data.model.Room;
import hu.sol.esme.rest.controller.AbstractRestService;
import hu.sol.esme.rest.controller.RoomRestService;
import hu.sol.esme.rest.type.ResponseMessageEnum;
import hu.sol.esme.rest.type.RestResponseBody;
import hu.sol.esme.service.transaction.RoomService;

@RestController
@RequestMapping("/rest/v1/room")
public class RoomRestServiceImpl extends AbstractRestService implements RoomRestService {

	private static final Logger LOG = LogManager.getLogger(RoomRestServiceImpl.class);

	@Autowired
	private RoomService roomService;

	@PostConstruct
	private void initRoomRest() {
		LOG.debug("init RoomRest Service");
	}

	@Override
	@RequestMapping(value = "/username/{username}", method = RequestMethod.GET, produces = {
			"application/json" }, headers = "Accept=*")
	public RestResponseBody<Room> listRoomByUserName(@PathVariable("username") String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@RequestMapping(value = "/email/{email}", method = RequestMethod.GET, produces = {
			"application/json" }, headers = "Accept=*")
	public RestResponseBody<Room> listRoomByUserEmail(@PathVariable("email") String email) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@RequestMapping(value = "/userid/{id}", method = RequestMethod.GET, produces = {
			"application/json" }, headers = "Accept=*")
	public RestResponseBody<Room> listRoomByUserId(@PathVariable("id") Long userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = { "application/json" }, headers = "Accept=*")
	public RestResponseBody<Room> listRoom() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = {
			"application/json" }, headers = "Accept=*")
	public RestResponseBody<Room> getRoomById(@PathVariable("id") Long roomId) {
		LOG.debug("getRoomById called.");

		RestResponseBody<Room> result = new RestResponseBody<Room>();

		Room response = roomService.getRoomById(roomId);

		if (response == null) {
			LOG.error("Room is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}
		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/create", method = RequestMethod.POST, produces = {
			"application/json" }, headers = "Accept=*")
	public RestResponseBody<Room> createRoom(@RequestBody Room room) {
		LOG.debug("getRoomById called.");

		RestResponseBody<Room> result = new RestResponseBody<Room>();

		Room response = roomService.saveRoom(room);

		if (response == null) {
			LOG.error("Room is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}
		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/update", method = RequestMethod.PUT, produces = {
			"application/json" }, headers = "Accept=*")
	public RestResponseBody<Room> updateRoom(Room room) {
		LOG.debug("updateRoom called.");

		RestResponseBody<Room> result = new RestResponseBody<Room>();

		Room response = roomService.updateRoom(room);

		if (response == null) {
			LOG.error("Room is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}
		
		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/updateOpeningTime", method = RequestMethod.POST, produces = {
			"application/json" }, headers = "Accept=*")
	public RestResponseBody<Room> setRoomOpeningTime(@RequestBody Room room) {
		// TODO Auto-generated method stub
		return null;
	}

}
