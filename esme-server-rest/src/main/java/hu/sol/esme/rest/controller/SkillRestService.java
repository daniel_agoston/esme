package hu.sol.esme.rest.controller;

import hu.sol.esme.data.model.IndustrySkill;
import hu.sol.esme.data.model.TechnicalSkill;
import hu.sol.esme.rest.type.RestResponseBody;

public interface SkillRestService {

	public RestResponseBody<TechnicalSkill> getTechnicalSkillById(Long id);

	public RestResponseBody<TechnicalSkill> getTechnicalSkillByIdWithGroup(Long id) throws Exception;

	public RestResponseBody<TechnicalSkill> getTechnicalSkillByIdWithDepth(Long id);

	public RestResponseBody<IndustrySkill> getIndustrySkilllById(Long id);

	public RestResponseBody<IndustrySkill> getIndustrySkillByIdWithGroup(Long id);

	public RestResponseBody<IndustrySkill> getIndustrySkillByIdWithDepth(Long id);

	public RestResponseBody<IndustrySkill> saveIndustrySkill(IndustrySkill industrySkill);

	public RestResponseBody<TechnicalSkill> saveTechnicalSkill(TechnicalSkill technicalSkill);
}
