package hu.sol.esme.rest.controller.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import hu.sol.esme.data.model.Project;
import hu.sol.esme.rest.controller.AbstractRestService;
import hu.sol.esme.rest.controller.ProjectRestService;
import hu.sol.esme.rest.type.ResponseMessageEnum;
import hu.sol.esme.rest.type.RestResponseBody;
import hu.sol.esme.service.transaction.ProjectService;

@RestController
@RequestMapping("/rest/v1/project")
public class ProjectRestServiceImpl extends AbstractRestService implements ProjectRestService {

	private static final Logger LOG = LogManager.getLogger(ProjectRestServiceImpl.class);

	@Autowired
	private ProjectService projectService;

	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = { "application/json",
			"application/xml" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<Project> getProjectById(@PathVariable("id") Long id) {
		LOG.debug("getProjectById called.");

		RestResponseBody<Project> result = new RestResponseBody<Project>();

		Project response = projectService.getProjectById(id);

		if (response == null) {
			LOG.error("Project is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().add(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = { "application/json",
			"application/xml" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<Project> listProjects() {
		LOG.debug("getProjectById called.");

		RestResponseBody<Project> result = new RestResponseBody<Project>();

		List<Project> response = projectService.listProject();

		if (CollectionUtils.isEmpty(response)) {
			LOG.error("Project is null.");
			setResponseMessage(result, ResponseMessageEnum.EMPTY);
			return result;
		}

		result.getResult().addAll(response);
		setResponseMessage(result, ResponseMessageEnum.SUCCESS);

		return result;
	}

	@Override
	@RequestMapping(value = "/name/{name}", method = RequestMethod.GET, produces = { "application/json",
			"application/xml" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<Project> listProjectsByName(@PathVariable("name") String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@RequestMapping(value = "/create", method = RequestMethod.POST, produces = { "application/json",
			"application/xml" }, headers = "Accept=*")
	public RestResponseBody<Project> saveProject(Project project) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@RequestMapping(value = "/update", method = RequestMethod.POST, produces = { "application/json",
			"application/xml" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<Project> updateProject(Project project) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = { "application/json",
			"application/xml" }, headers = "Accept=*")
	public @ResponseBody RestResponseBody<Project> deleteProject(@PathVariable("id") Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
