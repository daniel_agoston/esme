package hu.sol.esme.web.util.spring.provider;

import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;

import hu.sol.esme.web.util.spring.context.AppContext;

public class SpringBeanProvider {

	private static final Logger LOG = Logger.getLogger(SpringBeanProvider.class);

	public static <T> T getServiceFromContext(Class<T> clazz) {
		T bean = null;
		try {
			bean = AppContext.getApplicationContext().getBean(clazz);

		} catch (BeansException be) {
			LOG.warn("Warning: ", be);
		}
		return bean;
	}

	public static <T> T getServicesFromContext(Class<T> clazz) {
		Map<String, T> beanMap = null;
		try {
			beanMap = AppContext.getApplicationContext().getBeansOfType(clazz);

		} catch (BeansException be) {
			LOG.warn("Warning: ", be);
		}
		if (beanMap.entrySet().size() > 1) {
			LOG.warn("Warning: Too many object..");
			for (String elm : beanMap.keySet()) {
				LOG.debug(elm);
			}
			throw new RuntimeException("Too many implementation for this..");
		} else {
			System.out.println("Implementation size: " + beanMap.entrySet().size());
			for (Entry<String, T> entry : beanMap.entrySet()) {
				LOG.debug("ASD" + entry.getKey() + " " + entry.getValue());
			}
			Map.Entry<String, T> elm = beanMap.entrySet().iterator().next();
			LOG.debug("Returned service class: " + elm.getKey());
			return elm.getValue();
		}
	}

}
