
    alter table aspect_AUD 
        drop 
        foreign key FK_nxljlgmn7t7tiq5twkxjvgkdc;

    alter table aspect_opinion 
        drop 
        foreign key FK_dfkgsfo8b9o7jpfolosf555hn;

    alter table aspect_opinion 
        drop 
        foreign key FK_4kn154stm5m0kf7bhojch3hoc;

    alter table aspect_opinion 
        drop 
        foreign key FK_kmobkg53e7rnm5xheugbjgdxl;

    alter table aspect_opinion_AUD 
        drop 
        foreign key FK_mxm9nbq89w4wy9ci9r3ixs6t9;

    alter table comment 
        drop 
        foreign key FK_jk53lxhpavl1bxe40bas7psfk;

    alter table comment_AUD 
        drop 
        foreign key FK_b10ljr2926p6fl6vfnkp71acd;

    alter table depth_AUD 
        drop 
        foreign key FK_4wqroxf155w8slubi1lblr6cw;

    alter table industry_skill 
        drop 
        foreign key FK_oehlw5tixqxqsihaxqurikm8x;

    alter table industry_skill 
        drop 
        foreign key FK_fig7pvj292ehuue0iawc94mu6;

    alter table industry_skill_AUD 
        drop 
        foreign key FK_cj1w484awiwmcm9ccs51ulrl0;

    alter table industry_skill_grp_AUD 
        drop 
        foreign key FK_mnd79gtdxloe99dxg2lnqcqls;

    alter table opinion 
        drop 
        foreign key FK_dwg082x4w88hyl769pk0ap0jw;

    alter table opinion 
        drop 
        foreign key FK_bhek3iwt05c8j9kaprp19sb99;

    alter table opinion 
        drop 
        foreign key FK_uxfmxug9j36q2ty06iyq6uuf;

    alter table opinion_AUD 
        drop 
        foreign key FK_ofao4pl9eukgo5jvw6pac5koh;

    alter table project_AUD 
        drop 
        foreign key FK_n2xwgs7hevwt0ha611ftf085v;

    alter table room 
        drop 
        foreign key FK_b2j66kpe1di3kod5gqhp7hs92;

    alter table room 
        drop 
        foreign key FK_8gnpnkrj95h9k3pw45yv0vo8j;

    alter table room_AUD 
        drop 
        foreign key FK_rauk8q6kyvqyr8mvleyff47t6;

    alter table skill_AUD 
        drop 
        foreign key FK_oqw4nvbax21fsfyt0ss6fsdfi;

    alter table skill_depth 
        drop 
        foreign key FK_2mw1koebg6c740b7m018hvktx;

    alter table skill_depth 
        drop 
        foreign key FK_7kybhvc5fql21kl2oe5pe8eww;

    alter table skill_depth_AUD 
        drop 
        foreign key FK_rjd4la2kh2cr5r31a3jb1hqy0;

    alter table skill_opinion 
        drop 
        foreign key FK_5wslbi65k1xlxtpccv40cklny;

    alter table skill_opinion 
        drop 
        foreign key FK_j0mua230vyr7hwjxkw1tmoio5;

    alter table skill_opinion_AUD 
        drop 
        foreign key FK_428nva9fkll860h3f76176gn8;

    alter table techn_skill 
        drop 
        foreign key FK_jytn9n96pbx1mab2bm1gwj4u4;

    alter table techn_skill 
        drop 
        foreign key FK_mnmm79dr34gwa3emkooxseym9;

    alter table techn_skill_AUD 
        drop 
        foreign key FK_ryvrntey73wq3onitxmu7cqgc;

    alter table technical_skill_grp_AUD 
        drop 
        foreign key FK_r6ou3mevy136lpns0bdvowiv4;

    alter table user_AUD 
        drop 
        foreign key FK_ih31uc6fikcf93ek8i9xqfxxo;

    alter table user_role 
        drop 
        foreign key FK_aphxiciwirrvuc0y7y2s2rufj;

    alter table user_role_AUD 
        drop 
        foreign key FK_menp99k9u6aa71tfc3tb0ttmg;

    drop table if exists REVINFO;

    drop table if exists aspect;

    drop table if exists aspect_AUD;

    drop table if exists aspect_opinion;

    drop table if exists aspect_opinion_AUD;

    drop table if exists comment;

    drop table if exists comment_AUD;

    drop table if exists depth;

    drop table if exists depth_AUD;

    drop table if exists industry_skill;

    drop table if exists industry_skill_AUD;

    drop table if exists industry_skill_grp;

    drop table if exists industry_skill_grp_AUD;

    drop table if exists opinion;

    drop table if exists opinion_AUD;

    drop table if exists project;

    drop table if exists project_AUD;

    drop table if exists room;

    drop table if exists room_AUD;

    drop table if exists skill;

    drop table if exists skill_AUD;

    drop table if exists skill_depth;

    drop table if exists skill_depth_AUD;

    drop table if exists skill_opinion;

    drop table if exists skill_opinion_AUD;

    drop table if exists techn_skill;

    drop table if exists techn_skill_AUD;

    drop table if exists technical_skill_grp;

    drop table if exists technical_skill_grp_AUD;

    drop table if exists user;

    drop table if exists user_AUD;

    drop table if exists user_role;

    drop table if exists user_role_AUD;

    create table REVINFO (
        id integer not null auto_increment,
        DATE_OPER date,
        timestamp bigint not null,
        USER_NAME varchar(255),
        primary key (id)
    );

    create table aspect (
        id bigint not null auto_increment,
        description varchar(1000),
        name varchar(50),
        type varchar(150),
        primary key (id)
    );

    create table aspect_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        description varchar(1000),
        name varchar(50),
        type varchar(150),
        primary key (id, REV)
    );

    create table aspect_opinion (
        opinion_id bigint not null,
        aspect_id bigint,
        room_id bigint,
        primary key (opinion_id)
    );

    create table aspect_opinion_AUD (
        opinion_id bigint not null,
        REV integer not null,
        aspect_id bigint,
        room_id bigint,
        primary key (opinion_id, REV)
    );

    create table comment (
        id bigint not null auto_increment,
        text_content varchar(2000),
        opinion_id bigint,
        primary key (id)
    );

    create table comment_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        text_content varchar(2000),
        opinion_id bigint,
        primary key (id, REV)
    );

    create table depth (
        id bigint not null auto_increment,
        description varchar(1000),
        dgroup varchar(100),
        name varchar(100),
        primary key (id)
    );

    create table depth_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        description varchar(1000),
        dgroup varchar(100),
        name varchar(100),
        primary key (id, REV)
    );

    create table industry_skill (
        skill_id bigint not null,
        industrySkillGroup_id bigint,
        primary key (skill_id)
    );

    create table industry_skill_AUD (
        skill_id bigint not null,
        REV integer not null,
        industrySkillGroup_id bigint,
        primary key (skill_id, REV)
    );

    create table industry_skill_grp (
        id bigint not null auto_increment,
        description varchar(1000),
        name varchar(50),
        primary key (id)
    );

    create table industry_skill_grp_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        description varchar(1000),
        name varchar(50),
        primary key (id, REV)
    );

    create table opinion (
        id bigint not null auto_increment,
        comment_id bigint,
        created_by bigint,
        user_from bigint,
        primary key (id)
    );

    create table opinion_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        comment_id bigint,
        created_by bigint,
        user_from bigint,
        primary key (id, REV)
    );

    create table project (
        id bigint not null auto_increment,
        description varchar(2000),
        name varchar(100),
        primary key (id)
    );

    create table project_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        description varchar(2000),
        name varchar(100),
        primary key (id, REV)
    );

    create table room (
        id bigint not null auto_increment,
        name varchar(255),
        open_from datetime,
        open_to datetime,
        created_by bigint,
        project_id bigint,
        primary key (id)
    );

    create table room_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        name varchar(255),
        open_from datetime,
        open_to datetime,
        created_by bigint,
        project_id bigint,
        primary key (id, REV)
    );

    create table skill (
        id bigint not null auto_increment,
        description varchar(1000),
        name varchar(50),
        primary key (id)
    );

    create table skill_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        description varchar(1000),
        name varchar(50),
        primary key (id, REV)
    );

    create table skill_depth (
        id bigint not null auto_increment,
        depth_id bigint not null,
        skill_id bigint not null,
        primary key (id)
    );

    create table skill_depth_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        depth_id bigint,
        skill_id bigint,
        primary key (id, REV)
    );

    create table skill_opinion (
        opinion_id bigint not null,
        skillDepth_id bigint,
        primary key (opinion_id)
    );

    create table skill_opinion_AUD (
        opinion_id bigint not null,
        REV integer not null,
        skillDepth_id bigint,
        primary key (opinion_id, REV)
    );

    create table techn_skill (
        skill_id bigint not null,
        technicalSkillGroup_id bigint,
        primary key (skill_id)
    );

    create table techn_skill_AUD (
        skill_id bigint not null,
        REV integer not null,
        technicalSkillGroup_id bigint,
        primary key (skill_id, REV)
    );

    create table technical_skill_grp (
        id bigint not null auto_increment,
        description varchar(1000),
        name varchar(50),
        primary key (id)
    );

    create table technical_skill_grp_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        description varchar(1000),
        name varchar(50),
        primary key (id, REV)
    );

    create table user (
        id bigint not null auto_increment,
        accountNonExpired bit not null,
        accountNonLocked bit not null,
        credentialsNonExpired bit not null,
        email varchar(100) not null,
        enabled bit not null,
        gender varchar(32) default 'UNKNOWN',
        password varchar(200) not null,
        username varchar(45) not null,
        primary key (id)
    );

    create table user_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        accountNonExpired bit,
        accountNonLocked bit,
        credentialsNonExpired bit,
        email varchar(100),
        enabled bit,
        gender varchar(32) default 'UNKNOWN',
        password varchar(200),
        username varchar(45),
        primary key (id, REV)
    );

    create table user_role (
        user_role_id bigint not null auto_increment,
        role varchar(45) not null,
        username bigint not null,
        primary key (user_role_id)
    );

    create table user_role_AUD (
        user_role_id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        role varchar(45),
        username bigint,
        primary key (user_role_id, REV)
    );

    alter table skill_depth 
        add constraint UK_2mw1koebg6c740b7m018hvktx  unique (depth_id);

    alter table skill_depth 
        add constraint UK_7kybhvc5fql21kl2oe5pe8eww  unique (skill_id);

    alter table skill_depth 
        add constraint UK_1wy3ih0lmynx82u0rj8rdqblc  unique (skill_id, depth_id);

    alter table user 
        add constraint UK_sb8bbouer5wak8vyiiy4pf2bx  unique (username);

    alter table user_role 
        add constraint UK_adnyt6agwl65jnnokuvnskhn2  unique (role, username);

    alter table aspect_AUD 
        add constraint FK_nxljlgmn7t7tiq5twkxjvgkdc 
        foreign key (REV) 
        references REVINFO (id);

    alter table aspect_opinion 
        add constraint FK_dfkgsfo8b9o7jpfolosf555hn 
        foreign key (aspect_id) 
        references aspect (id);

    alter table aspect_opinion 
        add constraint FK_4kn154stm5m0kf7bhojch3hoc 
        foreign key (room_id) 
        references room (id);

    alter table aspect_opinion 
        add constraint FK_kmobkg53e7rnm5xheugbjgdxl 
        foreign key (opinion_id) 
        references opinion (id);

    alter table aspect_opinion_AUD 
        add constraint FK_mxm9nbq89w4wy9ci9r3ixs6t9 
        foreign key (opinion_id, REV) 
        references opinion_AUD (id, REV);

    alter table comment 
        add constraint FK_jk53lxhpavl1bxe40bas7psfk 
        foreign key (opinion_id) 
        references opinion (id);

    alter table comment_AUD 
        add constraint FK_b10ljr2926p6fl6vfnkp71acd 
        foreign key (REV) 
        references REVINFO (id);

    alter table depth_AUD 
        add constraint FK_4wqroxf155w8slubi1lblr6cw 
        foreign key (REV) 
        references REVINFO (id);

    alter table industry_skill 
        add constraint FK_oehlw5tixqxqsihaxqurikm8x 
        foreign key (industrySkillGroup_id) 
        references industry_skill_grp (id);

    alter table industry_skill 
        add constraint FK_fig7pvj292ehuue0iawc94mu6 
        foreign key (skill_id) 
        references skill (id);

    alter table industry_skill_AUD 
        add constraint FK_cj1w484awiwmcm9ccs51ulrl0 
        foreign key (skill_id, REV) 
        references skill_AUD (id, REV);

    alter table industry_skill_grp_AUD 
        add constraint FK_mnd79gtdxloe99dxg2lnqcqls 
        foreign key (REV) 
        references REVINFO (id);

    alter table opinion 
        add constraint FK_dwg082x4w88hyl769pk0ap0jw 
        foreign key (comment_id) 
        references comment (id);

    alter table opinion 
        add constraint FK_bhek3iwt05c8j9kaprp19sb99 
        foreign key (created_by) 
        references user (id);

    alter table opinion 
        add constraint FK_uxfmxug9j36q2ty06iyq6uuf 
        foreign key (user_from) 
        references user (id);

    alter table opinion_AUD 
        add constraint FK_ofao4pl9eukgo5jvw6pac5koh 
        foreign key (REV) 
        references REVINFO (id);

    alter table project_AUD 
        add constraint FK_n2xwgs7hevwt0ha611ftf085v 
        foreign key (REV) 
        references REVINFO (id);

    alter table room 
        add constraint FK_b2j66kpe1di3kod5gqhp7hs92 
        foreign key (created_by) 
        references user (id);

    alter table room 
        add constraint FK_8gnpnkrj95h9k3pw45yv0vo8j 
        foreign key (project_id) 
        references project (id);

    alter table room_AUD 
        add constraint FK_rauk8q6kyvqyr8mvleyff47t6 
        foreign key (REV) 
        references REVINFO (id);

    alter table skill_AUD 
        add constraint FK_oqw4nvbax21fsfyt0ss6fsdfi 
        foreign key (REV) 
        references REVINFO (id);

    alter table skill_depth 
        add constraint FK_2mw1koebg6c740b7m018hvktx 
        foreign key (depth_id) 
        references depth (id);

    alter table skill_depth 
        add constraint FK_7kybhvc5fql21kl2oe5pe8eww 
        foreign key (skill_id) 
        references skill (id);

    alter table skill_depth_AUD 
        add constraint FK_rjd4la2kh2cr5r31a3jb1hqy0 
        foreign key (REV) 
        references REVINFO (id);

    alter table skill_opinion 
        add constraint FK_5wslbi65k1xlxtpccv40cklny 
        foreign key (skillDepth_id) 
        references skill_depth (id);

    alter table skill_opinion 
        add constraint FK_j0mua230vyr7hwjxkw1tmoio5 
        foreign key (opinion_id) 
        references opinion (id);

    alter table skill_opinion_AUD 
        add constraint FK_428nva9fkll860h3f76176gn8 
        foreign key (opinion_id, REV) 
        references opinion_AUD (id, REV);

    alter table techn_skill 
        add constraint FK_jytn9n96pbx1mab2bm1gwj4u4 
        foreign key (technicalSkillGroup_id) 
        references technical_skill_grp (id);

    alter table techn_skill 
        add constraint FK_mnmm79dr34gwa3emkooxseym9 
        foreign key (skill_id) 
        references skill (id);

    alter table techn_skill_AUD 
        add constraint FK_ryvrntey73wq3onitxmu7cqgc 
        foreign key (skill_id, REV) 
        references skill_AUD (id, REV);

    alter table technical_skill_grp_AUD 
        add constraint FK_r6ou3mevy136lpns0bdvowiv4 
        foreign key (REV) 
        references REVINFO (id);

    alter table user_AUD 
        add constraint FK_ih31uc6fikcf93ek8i9xqfxxo 
        foreign key (REV) 
        references REVINFO (id);

    alter table user_role 
        add constraint FK_aphxiciwirrvuc0y7y2s2rufj 
        foreign key (username) 
        references user (id);

    alter table user_role_AUD 
        add constraint FK_menp99k9u6aa71tfc3tb0ttmg 
        foreign key (REV) 
        references REVINFO (id);
