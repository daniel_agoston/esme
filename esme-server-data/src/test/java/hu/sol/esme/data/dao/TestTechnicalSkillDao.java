package hu.sol.esme.data.dao;

import java.io.Serializable;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import hu.sol.esme.data.model.TechnicalSkill;
import hu.sol.esme.data.model.TechnicalSkillGroup;
import io.github.benas.randombeans.EnhancedRandomBuilder;
import io.github.benas.randombeans.api.EnhancedRandom;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-datasource-config.xml" })
public class TestTechnicalSkillDao extends AbstractTestDao<TechnicalSkillDao> {

	private static final Logger LOG = LogManager.getLogger(TestTechnicalSkillDao.class);

	@Autowired
	private TechnicalSkillGroupDao technicalSkillGroupDao;

	@Test
	@Transactional
	public void getSkillByIdNotFound() {
		TechnicalSkill technicalSkill = getDao().find(new Long(-1));

		Assert.assertNull("No result expected", technicalSkill);
	}

	@Test
	@Transactional
	@Commit
	public void saveTechnicalSkill() {
		EnhancedRandom enhancedRandom = EnhancedRandomBuilder.aNewEnhancedRandomBuilder().build();
		TechnicalSkill technicalSkill = enhancedRandom.nextObject(TechnicalSkill.class, "id");
		TechnicalSkillGroup technicalSkillGroup = technicalSkillGroupDao.find(new Long(1));
		technicalSkill.setTechnicalSkillGroup(technicalSkillGroup);
		Serializable id = getDao().merge(technicalSkill);
		Assert.assertNotNull("Result expected", id);

	}

	@Test
	@Transactional
	@Commit
	public void saveTechnicalSkillWithGroup() {
		EnhancedRandom enhancedRandom = EnhancedRandomBuilder.aNewEnhancedRandomBuilder().build();
		TechnicalSkill technicalSkill = enhancedRandom.nextObject(TechnicalSkill.class, "id");
		TechnicalSkillGroup technicalSkillGroup = enhancedRandom.nextObject(TechnicalSkillGroup.class, "id",
				"technicalSkills");
		technicalSkill.setTechnicalSkillGroup(technicalSkillGroup);
		Serializable id = getDao().merge(technicalSkill);
		Assert.assertNotNull("Result expected", id);

	}
}
