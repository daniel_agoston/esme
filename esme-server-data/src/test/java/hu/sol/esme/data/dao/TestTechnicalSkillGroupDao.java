package hu.sol.esme.data.dao;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import hu.sol.esme.data.model.TechnicalSkillGroup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-datasource-config.xml" })
public class TestTechnicalSkillGroupDao extends AbstractTestDao<TechnicalSkillGroupDao> {

	private static final Logger LOG = LogManager.getLogger(TestTechnicalSkillDao.class);

	@Test
	@Transactional(readOnly = true)
	public void getSkillGroupById_Lazy() {
		LOG.info("getSkillGroupById_Lazy");
		TechnicalSkillGroup technicalSkillGroup = getDao().find(new Long(1));
		Assert.assertEquals("No result expected", 0, 0);
	}

	@Test
	@Transactional(readOnly = true)
	public void getSkillGroupById_FetchOverride() {
		LOG.info("getSkillById_FetchOverride");
		TechnicalSkillGroup technicalSkillGroup = getDao().loadTechnicalSkillGroupByIdWithSkills(new Long(1));

		Assert.assertNotEquals("No result expected", 0, technicalSkillGroup.getTechnicalSkills().size());
	}
}
