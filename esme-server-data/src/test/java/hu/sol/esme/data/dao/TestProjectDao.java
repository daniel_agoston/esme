package hu.sol.esme.data.dao;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.thoughtworks.xstream.XStream;

import hu.sol.esme.data.model.Project;
import io.github.benas.randombeans.EnhancedRandomBuilder;
import io.github.benas.randombeans.api.EnhancedRandom;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-datasource-config.xml" })
public class TestProjectDao extends AbstractTestDao<ProjectDao> {

	private static final Logger LOG = LogManager.getLogger(TestProjectDao.class);

	@Test
	@Transactional(readOnly = true)
	public void getByIdNotFound() {
		Project project = getDao().find(new Long(-1));

		assertNull("No result expected", project);
	}

	@Test
	@Transactional
	@Commit
	public void saveProject() {
		EnhancedRandom enhancedRandom = EnhancedRandomBuilder.aNewEnhancedRandomBuilder().build();
		Project project = enhancedRandom.nextObject(Project.class, "id");
		getDao().persist(project);

	}

	@Test
	@Transactional(readOnly = true)
	public void listAllNotFound() {
		List<Project> project = getDao().listAll();

		assertThat(project, is(not(empty())));
	}

	@Test
	@Transactional
	@Commit
	public void saveProjectAndFind() {
		EnhancedRandom enhancedRandom = EnhancedRandomBuilder.aNewEnhancedRandomBuilder().build();
		Project project = enhancedRandom.nextObject(Project.class, "id");
		Project savedProject = getDao().merge(project);
		LOG.error(new XStream().toXML(savedProject));
		Project projectFound = getDao().find(savedProject.getId());

		assertNotNull("Result expected", projectFound);
	}

	@Test
	@Transactional
	@Commit
	public void saveProjectAndUpdate() {
		// EnhancedRandom enhancedRandom =
		// EnhancedRandomBuilder.aNewEnhancedRandomBuilder().build();
		// Project project = enhancedRandom.nextObject(Project.class, "id");
		// Project savedProject = getDao().merge(project);
		Project savedProject = getDao().find(new Long(1));
		
		LOG.error(new XStream().toXML(savedProject));
		
		savedProject.setName("ProjectName");
		getDao().update(savedProject);

		assertThat(savedProject, is(not(nullValue())));
		assertNotNull("Result expected", savedProject);
	}

	@Test
	@Transactional
	@Commit
	public void updateProject() {
		Project project = getDao().listProjectByName("ProjectName").get(0);
		project.setName("Az új név");

		getDao().update(project);

		Project updatedProject = getDao().listProjectByName("Az új név").get(0);

		assertEquals(project, updatedProject);
	}

}
