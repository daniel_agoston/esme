package hu.sol.esme.data.dao;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.io.Serializable;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import hu.sol.esme.data.model.Depth;
import hu.sol.esme.data.model.Skill;
import hu.sol.esme.data.model.SkillDepth;
import io.github.benas.randombeans.EnhancedRandomBuilder;
import io.github.benas.randombeans.api.EnhancedRandom;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-datasource-config.xml" })
public class TestSkillDao extends AbstractTestDao<SkillDao> {

	private static final Logger LOG = LogManager.getLogger(TestSkillDao.class);

	@Test
	@Transactional
	public void getSkillByIdNotFound() {
		Skill skill = getDao().find(new Long(-1));

		assertNull("No result expected", skill);
	}

	@Test
	@Commit
	@Transactional
	public void saveSkill() {
		EnhancedRandom enhancedRandom = EnhancedRandomBuilder.aNewEnhancedRandomBuilder().build();
		Skill skill = enhancedRandom.nextObject(Skill.class, "id");
		Serializable id = getDao().merge(skill);
		assertNotNull("Result expected", id);

	}

	@Test
	@Commit
	@Transactional
	public void saveSkillWithDepth() {
		EnhancedRandom enhancedRandom = EnhancedRandomBuilder.aNewEnhancedRandomBuilder().build();
		Skill skill = enhancedRandom.nextObject(Skill.class, "id");
		SkillDepth skillDepth = enhancedRandom.nextObject(SkillDepth.class, "id");
		Depth depth = enhancedRandom.nextObject(Depth.class, "id");

		skillDepth.setDepth(depth);
		skill.setSkillDepth(skillDepth);
		Serializable id = getDao().merge(skill);

		assertNotNull("Result expected", id);

	}
}
