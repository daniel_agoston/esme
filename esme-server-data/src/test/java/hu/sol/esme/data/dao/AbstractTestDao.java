package hu.sol.esme.data.dao;

import org.springframework.beans.factory.annotation.Autowired;

import junit.framework.TestCase;

public class AbstractTestDao<T> extends TestCase {

	@Autowired
	private T dao;

	public T getDao() {
		return dao;
	}

}
