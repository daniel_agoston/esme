package hu.sol.esme.data.dao;

import java.io.Serializable;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import hu.sol.esme.data.model.Depth;
import hu.sol.esme.data.model.Skill;
import hu.sol.esme.data.model.SkillDepth;
import io.github.benas.randombeans.EnhancedRandomBuilder;
import io.github.benas.randombeans.api.EnhancedRandom;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-datasource-config.xml" })
public class TestSkillDepthDao extends AbstractTestDao<SkillDepthDao> {

	private static final Logger LOG = LogManager.getLogger(TestSkillDepthDao.class);

	@Test
	@Transactional
	public void getSkillByIdNotFound() {
		SkillDepth skillDepth = getDao().find(new Long(-1));

		assertNull("No result expected", skillDepth);
	}

	@Test
	@Commit
	@Transactional
	public void saveSkill() {
		EnhancedRandom enhancedRandom = EnhancedRandomBuilder.aNewEnhancedRandomBuilder().build();
		SkillDepth skillDepth = enhancedRandom.nextObject(SkillDepth.class, "id");
		Skill skill = enhancedRandom.nextObject(Skill.class, "id");
		Depth depth = enhancedRandom.nextObject(Depth.class, "id");
		skillDepth.setSkill(skill);
		skillDepth.setDepth(depth);
		Serializable id = getDao().merge(skillDepth);
		assertNotNull("Result expected", id);

	}
}
