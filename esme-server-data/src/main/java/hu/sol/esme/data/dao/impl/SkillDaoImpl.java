package hu.sol.esme.data.dao.impl;

import java.io.Serializable;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import hu.sol.esme.data.dao.SkillDao;
import hu.sol.esme.data.model.Skill;

@Repository
public class SkillDaoImpl extends GenericDaoImpl<Skill, Serializable> implements SkillDao {

	private static final Logger LOG = LogManager.getLogger(SkillDaoImpl.class);

}
