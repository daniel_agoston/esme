package hu.sol.esme.data.dao;

import hu.sol.esme.data.model.User;

public interface UserDao {
	
	public User findByUserName(String username);

	public void registerUser(User user);

	public void updateUser(User user);
}
