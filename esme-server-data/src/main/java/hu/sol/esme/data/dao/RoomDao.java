package hu.sol.esme.data.dao;

import java.io.Serializable;

import hu.sol.esme.data.model.Room;

public interface RoomDao extends GenericDao<Room, Serializable> {

}
