package hu.sol.esme.data.dao.impl;

import java.io.Serializable;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import hu.sol.esme.data.dao.IndustrySkillDao;
import hu.sol.esme.data.model.IndustrySkill;

@Repository
public class IndustrySkillDaoImpl extends GenericDaoImpl<IndustrySkill, Serializable> implements IndustrySkillDao {

	private static final Logger LOG = LogManager.getLogger(IndustrySkillDaoImpl.class);

	@Override
	public IndustrySkill loadIndustrySkillByIdWithGroup(Long id) {
		currentSession().enableFetchProfile("loadIndustrySkillWithGroup");
		LOG.error(id);
		IndustrySkill industrySkill = currentSession().get(IndustrySkill.class, id);
		currentSession().disableFetchProfile("loadIndustrySkillWithGroup");

		return industrySkill;
	}

}
