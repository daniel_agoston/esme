package hu.sol.esme.data.dao;

import java.io.Serializable;

import hu.sol.esme.data.model.SkillDepth;

public interface SkillDepthDao extends GenericDao<SkillDepth, Serializable> {

}
