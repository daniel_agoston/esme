package hu.sol.esme.data.dao;

import java.io.Serializable;

import hu.sol.esme.data.model.TechnicalSkillGroup;

public interface TechnicalSkillGroupDao extends GenericDao<TechnicalSkillGroup, Serializable> {

	public TechnicalSkillGroup loadTechnicalSkillGroupByIdWithSkills(Long id);
}
