package hu.sol.esme.data.model;

public enum Gender {
	MALE, FEMALE, UNKNOWN;
}
