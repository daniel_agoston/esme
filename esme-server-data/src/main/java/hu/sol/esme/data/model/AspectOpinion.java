package hu.sol.esme.data.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.FetchProfile;
import org.hibernate.annotations.FetchProfiles;
import org.hibernate.envers.Audited;
import org.springframework.stereotype.Component;

@FetchProfiles(value = { @FetchProfile(name = "loadAspectOpinionWithRoom", fetchOverrides = {
		@FetchProfile.FetchOverride(association = "room", entity = AspectOpinion.class, mode = FetchMode.JOIN) }) })
@Component
@Entity
@Audited
@Table(name = "aspect_opinion")
@PrimaryKeyJoinColumn(name = "opinion_id", referencedColumnName = "id")
public class AspectOpinion extends Opinion implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.LAZY)
	private Aspect aspect;

	@ManyToOne(fetch = FetchType.LAZY)
	private Room room;

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public Aspect getAspect() {
		return aspect;
	}

	public void setAspect(Aspect aspect) {
		this.aspect = aspect;
	}

}
