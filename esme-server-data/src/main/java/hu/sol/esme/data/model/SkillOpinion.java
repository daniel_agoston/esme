package hu.sol.esme.data.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.FetchProfile;
import org.hibernate.annotations.FetchProfiles;
import org.hibernate.envers.Audited;
import org.springframework.stereotype.Component;

@FetchProfiles(value = { @FetchProfile(name = "loadSkillOpinionWithSkillDepth", fetchOverrides = {
		@FetchProfile.FetchOverride(association = "skillDepth", entity = SkillOpinion.class, mode = FetchMode.JOIN) }) })
@Component
@Entity
@Audited
@Table(name = "skill_opinion")
@PrimaryKeyJoinColumn(name = "opinion_id", referencedColumnName = "id")
public class SkillOpinion extends Opinion implements Serializable {

	private static final long serialVersionUID = 1L;

	@OneToOne(fetch = FetchType.LAZY)
	private SkillDepth skillDepth;

	public SkillDepth getSkillDepth() {
		return skillDepth;
	}

	public void setSkillDepth(SkillDepth skillDepth) {
		this.skillDepth = skillDepth;
	}
}
