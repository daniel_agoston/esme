package hu.sol.esme.data.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
@Entity
@Audited
@Table(name = "user")
public class User implements UserDetails, Serializable {

	private static final long serialVersionUID = 5441144395049253519L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "username", unique = true, nullable = false, length = 45)
	private String username;

	@Column(name = "password", nullable = false, length = 200)
	private String password;

	@Column(name = "email", nullable = false, length = 100)
	private String email;

	@Column(name = "enabled", nullable = false)
	private boolean enabled;

	@Column(name = "accountNonExpired", nullable = false)
	private boolean accountNonExpired;

	@Column(name = "accountNonLocked", nullable = false)
	private boolean accountNonLocked;

	@Column(name = "credentialsNonExpired", nullable = false)
	private boolean credentialsNonExpired;

	@Enumerated(EnumType.STRING)
	@Column(length = 32, columnDefinition = "varchar(32) default 'UNKNOWN'")
	private Gender gender = Gender.UNKNOWN;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "user", cascade = CascadeType.ALL)
	private Set<UserRole> authority = new HashSet<UserRole>(0);

	public User() {
	}

	public User(String username, String password, String email, boolean enabled) {
		this.username = username;
		this.password = password;
		this.email = email;
		this.enabled = enabled;
	}

	public User(String username, String password, String email, boolean enabled, Set<UserRole> userRole) {
		this.username = username;
		this.password = password;
		this.email = email;
		this.enabled = enabled;
		this.authority = userRole;
	}

	public User(String username, String password, String email, Gender gender, boolean enabled, Set<UserRole> userRole,
			boolean accountNonExpired, boolean accountNonLocked, boolean credentialsNonExpired) {
		this.username = username;
		this.password = password;
		this.email = email;
		this.gender = gender;
		this.enabled = enabled;
		this.authority = userRole;
		this.accountNonExpired = accountNonExpired;
		this.accountNonLocked = accountNonLocked;
		this.credentialsNonExpired = credentialsNonExpired;
	}

	// GETTERS and SETTERS

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public boolean isEnabled() {
		return this.enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isAccountNonExpired() {
		return this.accountNonExpired;
	}

	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	public boolean isAccountNonLocked() {
		return this.accountNonLocked;
	}

	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	public boolean isCredentialsNonExpired() {
		return this.credentialsNonExpired;
	}

	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	public void setAuthorities(Set<UserRole> userRole) {
		this.authority = userRole;
	}

	public Set<UserRole> getAuthorities() {
		return this.authority;
	}
}