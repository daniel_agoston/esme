package hu.sol.esme.data.dao;

import java.io.Serializable;
import java.util.List;

import hu.sol.esme.data.model.Aspect;

public interface AspectDao extends GenericDao<Aspect, Serializable> {

	public List<Aspect> listByName(String name);

}
