package hu.sol.esme.data.dao;

import java.io.Serializable;

import hu.sol.esme.data.model.UserRole;

public interface UserRoleDao extends GenericDao<UserRole, Serializable> {

}
