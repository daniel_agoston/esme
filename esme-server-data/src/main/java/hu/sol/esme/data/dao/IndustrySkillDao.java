package hu.sol.esme.data.dao;

import java.io.Serializable;

import hu.sol.esme.data.model.IndustrySkill;

public interface IndustrySkillDao extends GenericDao<IndustrySkill, Serializable> {

	public IndustrySkill loadIndustrySkillByIdWithGroup(Long id);

}
