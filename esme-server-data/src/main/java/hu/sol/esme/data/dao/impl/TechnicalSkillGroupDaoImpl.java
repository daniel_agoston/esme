package hu.sol.esme.data.dao.impl;

import java.io.Serializable;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.thoughtworks.xstream.XStream;

import hu.sol.esme.data.dao.TechnicalSkillGroupDao;
import hu.sol.esme.data.model.TechnicalSkillGroup;

@Repository
public class TechnicalSkillGroupDaoImpl extends GenericDaoImpl<TechnicalSkillGroup, Serializable>
		implements TechnicalSkillGroupDao {

	private static final Logger LOG = LogManager.getLogger(TechnicalSkillGroupDaoImpl.class);

	@Override
	public TechnicalSkillGroup loadTechnicalSkillGroupByIdWithSkills(Long id) {
		
		currentSession().enableFetchProfile("loadTechSkillGroupWithSkills");
		TechnicalSkillGroup technicalSkillGroup = currentSession().get(TechnicalSkillGroup.class, id);
		LOG.error(new XStream().toXML(technicalSkillGroup));
		currentSession().disableFetchProfile("loadTechSkillGroupWithSkills");

		return technicalSkillGroup;
	}

}
