package hu.sol.esme.data.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.envers.Audited;
import org.springframework.stereotype.Component;

@Component
@Entity
@Audited
@Table(name = "depth")
public class Depth implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "name")
	@Size(max = 100, message = "A mélység elnevezése nem lehet több {max} karakternél.")
	private String name;

	@Column(name = "dgroup")
	@Size(max = 100, message = "A mélység csoportja nem lehet több {max} karakternél.")
	private String dgroup;

	@Column(name = "description")
	@Size(max = 1000, message = "A mélység leírása nem lehet több {max} karakternél.")
	private String description;

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "depth")
	@Cascade({ CascadeType.SAVE_UPDATE })
	private SkillDepth skillDepth;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDgroup() {
		return dgroup;
	}

	public void setDgroup(String dgroup) {
		this.dgroup = dgroup;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public SkillDepth getSkillDepth() {
		return skillDepth;
	}

	public void setSkillDepth(SkillDepth skillDepth) {
		this.skillDepth = skillDepth;
	}

}
