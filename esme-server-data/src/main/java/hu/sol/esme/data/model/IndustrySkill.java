package hu.sol.esme.data.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.FetchProfile;
import org.hibernate.annotations.FetchProfiles;
import org.hibernate.envers.Audited;
import org.springframework.stereotype.Component;

@FetchProfiles(value = { @FetchProfile(name = "loadIndustrySkillWithGroup", fetchOverrides = {
		@FetchProfile.FetchOverride(association = "industrySkillGroup", entity = IndustrySkill.class, mode = FetchMode.JOIN) }) })
@Component
@Entity
@Audited
@Table(name = "industry_skill")
@PrimaryKeyJoinColumn(name = "skill_id", referencedColumnName = "id")
public class IndustrySkill extends Skill implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade({ CascadeType.SAVE_UPDATE })
	private IndustrySkillGroup industrySkillGroup;

	public IndustrySkillGroup getIndustrySkillGroup() {
		return industrySkillGroup;
	}

	public void setIndustrySkillGroup(IndustrySkillGroup industrySkillGroup) {
		this.industrySkillGroup = industrySkillGroup;
	}
}
