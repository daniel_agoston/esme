package hu.sol.esme.data.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import hu.sol.esme.data.dao.SkillOpinionDao;
import hu.sol.esme.data.model.SkillOpinion;

@Repository
public class SkillOpinionDaoImpl extends GenericDaoImpl<SkillOpinion, Serializable> implements SkillOpinionDao {

	private static final Logger LOG = LogManager.getLogger(SkillOpinionDaoImpl.class);

	@SuppressWarnings("unchecked")
	@Override
	public List<SkillOpinion> listSkillOpinionByCreator(String creator) {
		Criteria criteria = currentSession().createCriteria(daoType);

		//@formatter:off
		criteria.createAlias("createdBy", "createdBy")
				.add(Restrictions.like("createdBy.username", creator))
				.addOrder(Order.desc("createdBy.username"))
				.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);;
		//@formatter:on

		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SkillOpinion> listSkillOpinionByName(String name) {
		Criteria criteria = currentSession().createCriteria(daoType);

		//@formatter:off
		criteria.createAlias("user", "user")
				.add(Restrictions.like("user.username", name))
				.addOrder(Order.desc("user.username"))
				.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);;
		//@formatter:on

		return criteria.list();
	}
}
