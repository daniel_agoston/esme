package hu.sol.esme.data.dao;

import java.util.List;

/**
 * 
 * @author Daniel Agoston
 *
 * @param <E>
 *            - entity
 * @param <K>
 *            - serialized key
 */
public interface GenericDao<E, K> {

	public void save(E entity);
	
	public void persist(E entity);
	
	public E merge(E entity);

	public void update(E entity);

	public void remove(E entity);

	public E find(K key);

	public List<E> listAll();

}
