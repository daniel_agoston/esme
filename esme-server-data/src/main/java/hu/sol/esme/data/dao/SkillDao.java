package hu.sol.esme.data.dao;

import java.io.Serializable;

import hu.sol.esme.data.model.Skill;

public interface SkillDao extends GenericDao<Skill, Serializable> {

}
