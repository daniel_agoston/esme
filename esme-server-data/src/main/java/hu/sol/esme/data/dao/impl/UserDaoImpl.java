package hu.sol.esme.data.dao.impl;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import hu.sol.esme.data.dao.UserDao;
import hu.sol.esme.data.model.User;

@Repository
public class UserDaoImpl implements UserDao {

	private static final Logger LOG = LogManager.getLogger(UserDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public User findByUserName(String username) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("findByUserName()");
		}

		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.ilike("username", username));

		List<User> users = criteria.list();
		if (users.size() == 0) {
			return null;
		} else {
			return (User) criteria.list().get(0);
		}
	}

	@Override
	public void registerUser(User user) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("registerUser()");
		}
		sessionFactory.getCurrentSession().save(user);
	}

	@Override
	public void updateUser(User user) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("updateUser()");
		}

		sessionFactory.getCurrentSession().update(user);

	}
}
