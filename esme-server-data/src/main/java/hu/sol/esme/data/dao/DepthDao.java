package hu.sol.esme.data.dao;

import java.io.Serializable;
import java.util.List;

import hu.sol.esme.data.model.Depth;

public interface DepthDao extends GenericDao<Depth, Serializable> {

	public List<Depth> listDepthByGroup(String group);

}
