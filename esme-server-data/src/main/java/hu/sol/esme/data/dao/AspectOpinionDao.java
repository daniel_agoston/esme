package hu.sol.esme.data.dao;

import java.io.Serializable;
import java.util.List;

import hu.sol.esme.data.model.AspectOpinion;

public interface AspectOpinionDao extends GenericDao<AspectOpinion, Serializable> {

	public List<AspectOpinion> listAspectOpinionByCreator(String creator);

	public List<AspectOpinion> listAspectOpinionByName(String name);
}
