package hu.sol.esme.data.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import hu.sol.esme.data.dao.OpinionDao;
import hu.sol.esme.data.model.Opinion;

@Repository
public class OpinionDaoImpl extends GenericDaoImpl<Opinion, Serializable> implements OpinionDao {

	private static final Logger LOG = LogManager.getLogger(OpinionDaoImpl.class);

	@SuppressWarnings("unchecked")
	@Override
	public List<Opinion> listOpinionByCreator(String creator) {
		Criteria criteria = currentSession().createCriteria(daoType);

		//@formatter:off
		criteria.createAlias("createdBy", "createdBy")
				.add(Restrictions.like("createdBy.username", creator))
				.addOrder(Order.desc("createdBy.username"))
				.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);;
		//@formatter:on

		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Opinion> listOpinionByName(String name) {
		Criteria criteria = currentSession().createCriteria(daoType);

		//@formatter:off
		criteria.createAlias("user", "user")
				.add(Restrictions.like("user.username", name))
				.addOrder(Order.desc("user.username"))
				.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);;
		//@formatter:on

		return criteria.list();
	}
}
