package hu.sol.esme.data.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import hu.sol.esme.data.dao.DepthDao;
import hu.sol.esme.data.model.Depth;

@Repository
public class DepthDaoImpl extends GenericDaoImpl<Depth, Serializable> implements DepthDao {

	private static final Logger LOG = LogManager.getLogger(DepthDaoImpl.class);

	@SuppressWarnings("unchecked")
	@Override
	public List<Depth> listDepthByGroup(String group) {
		Criteria criteria = currentSession().createCriteria(daoType);

		//@formatter:off
		criteria.add(Restrictions.like("dgroup", group))
				.addOrder(Order.desc("name"));
		//@formatter:on

		return criteria.list();
	}

}
