package hu.sol.esme.data.dao;

import java.io.Serializable;
import java.util.List;

import hu.sol.esme.data.model.Project;

public interface ProjectDao extends GenericDao<Project, Serializable> {

	List<Project> listProjectByName(String name);

}
