package hu.sol.esme.data.dao.impl;

import java.io.Serializable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import hu.sol.esme.data.dao.CommentDao;
import hu.sol.esme.data.model.Comment;

@Repository
public class CommentDaoImpl extends GenericDaoImpl<Comment, Serializable> implements CommentDao {

	private static final Logger LOG = LogManager.getLogger(CommentDaoImpl.class);
}
