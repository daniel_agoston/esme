package hu.sol.esme.data.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import hu.sol.esme.data.dao.ProjectDao;
import hu.sol.esme.data.model.Project;

@Repository
public class ProjectDaoImpl extends GenericDaoImpl<Project, Serializable> implements ProjectDao {

	private static final Logger LOG = LogManager.getLogger(ProjectDaoImpl.class);

	@SuppressWarnings("unchecked")
	@Override
	public List<Project> listProjectByName(String name) {
		Criteria criteria = currentSession().createCriteria(daoType);

		//@formatter:off
		criteria.add(Restrictions.like("name", name))
				.addOrder(Order.desc("name"));
		//@formatter:on

		return criteria.list();
	}
}
