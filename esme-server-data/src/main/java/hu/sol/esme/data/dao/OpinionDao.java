package hu.sol.esme.data.dao;

import java.io.Serializable;
import java.util.List;

import hu.sol.esme.data.model.Opinion;

public interface OpinionDao extends GenericDao<Opinion, Serializable> {

	public List<Opinion> listOpinionByCreator(String creator);

	public List<Opinion> listOpinionByName(String name);

}
