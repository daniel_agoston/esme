package hu.sol.esme.data.dao.impl;

import java.io.Serializable;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import hu.sol.esme.data.dao.UserRoleDao;
import hu.sol.esme.data.model.UserRole;

@Repository
public class UserRoleDaoImpl extends GenericDaoImpl<UserRole, Serializable> implements UserRoleDao {

	private static final Logger LOG = LogManager.getLogger(UserRoleDaoImpl.class);
}
