package hu.sol.esme.data.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.FetchProfile;
import org.hibernate.annotations.FetchProfiles;
import org.hibernate.envers.Audited;
import org.springframework.stereotype.Component;

@FetchProfiles(value = { @FetchProfile(name = "loadIndustrySkillGroupWithSkils", fetchOverrides = {
		@FetchProfile.FetchOverride(association = "industrySkills", entity = IndustrySkillGroup.class, mode = FetchMode.JOIN) }) })
@Component
@Entity
@Audited
@Table(name = "industry_skill_grp")
public class IndustrySkillGroup implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "name")
	@Size(max = 50, message = "Az üzleti kategória neve nem lehet több {max} karakternél.")
	private String name;

	@Column(name = "description")
	@Size(max = 1000, message = "Az üzleti kategória leírása nem lehet több {max} karakternél.")
	private String description;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "industrySkillGroup")
	@Cascade({ CascadeType.SAVE_UPDATE })
	private Set<IndustrySkill> industrySkills = new HashSet<IndustrySkill>(0);

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<IndustrySkill> getIndustrySkills() {
		return industrySkills;
	}

	public void setIndustrySkills(Set<IndustrySkill> industrySkills) {
		this.industrySkills = industrySkills;
	}

}
