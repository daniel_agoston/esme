package hu.sol.esme.data.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import hu.sol.esme.data.dao.AspectOpinionDao;
import hu.sol.esme.data.model.AspectOpinion;

@Repository
public class AspectOpinionDaoImpl extends GenericDaoImpl<AspectOpinion, Serializable> implements AspectOpinionDao {

	private static final Logger LOG = LogManager.getLogger(AspectOpinionDaoImpl.class);

	@SuppressWarnings("unchecked")
	@Override
	public List<AspectOpinion> listAspectOpinionByCreator(String creator) {
		Criteria criteria = currentSession().createCriteria(daoType);

		//@formatter:off
		criteria.createAlias("createdBy", "createdBy")
				.add(Restrictions.like("createdBy.username", creator))
				.addOrder(Order.desc("createdBy.username"))
				.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);;
		//@formatter:on

		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AspectOpinion> listAspectOpinionByName(String name) {
		Criteria criteria = currentSession().createCriteria(daoType);

		//@formatter:off
		criteria.createAlias("user", "user")
				.add(Restrictions.like("user.username", name))
				.addOrder(Order.desc("user.username"))
				.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);;
		//@formatter:on

		return criteria.list();
	}

}
