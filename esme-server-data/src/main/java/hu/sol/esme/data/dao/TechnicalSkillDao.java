package hu.sol.esme.data.dao;

import java.io.Serializable;

import hu.sol.esme.data.model.TechnicalSkill;

public interface TechnicalSkillDao extends GenericDao<TechnicalSkill, Serializable> {

	public TechnicalSkill loadTechnicalSkillByIdWithGroup(Long id);
}
