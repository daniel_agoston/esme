package hu.sol.esme.data.dao;

import java.io.Serializable;

import hu.sol.esme.data.model.IndustrySkillGroup;

public interface IndustrySkillGroupDao extends GenericDao<IndustrySkillGroup, Serializable> {

	public IndustrySkillGroup loadIndustrySkillGroupByIdWithSkills(Long id);
}
