package hu.sol.esme.data.dao.impl;

import java.io.Serializable;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import hu.sol.esme.data.dao.SkillDepthDao;
import hu.sol.esme.data.model.SkillDepth;

@Repository
public class SkillDepthDaoImpl extends GenericDaoImpl<SkillDepth, Serializable> implements SkillDepthDao {

	private static final Logger LOG = LogManager.getLogger(SkillDepthDaoImpl.class);
}
