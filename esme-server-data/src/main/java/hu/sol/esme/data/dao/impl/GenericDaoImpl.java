package hu.sol.esme.data.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.Transient;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import hu.sol.esme.data.dao.GenericDao;

@Repository
public abstract class GenericDaoImpl<E, K extends Serializable> implements GenericDao<E, K> {

	@Autowired
	private SessionFactory sessionFactory;

	protected Class<? extends E> daoType;

	@SuppressWarnings("unchecked")
	public GenericDaoImpl() {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		daoType = (Class<? extends E>) pt.getActualTypeArguments()[0];
	}

	protected Session currentSession() {
		return sessionFactory.getCurrentSession();
	}

	/**
	 * save or update object in instance
	 * 
	 */
	@Override
	@Transient
	public void save(E entity) {
		currentSession().saveOrUpdate(entity);
	}

	/**
	 * merge objects in instance and save them
	 * 
	 */
	@Override
	@Transient
	@SuppressWarnings("unchecked")
	public E merge(E entity) {
		return (E) currentSession().merge(entity);
	}

	/**
	 * persist objects in instance and save them
	 * 
	 */
	@Override
	@Transient
	public void persist(E entity) {
		currentSession().persist(entity);
	}

	@Override
	@Transient
	public void update(E entity) {
		currentSession().update(entity);
	}

	@Override
	@Transient
	public void remove(E entity) {
		currentSession().delete(entity);
	}

	@Override
	@Transient
	public E find(K key) {
		return (E) currentSession().get(daoType, key);
	}

	@Override
	@Transient
	@SuppressWarnings("unchecked")
	public List<E> listAll() {
		return currentSession().createCriteria(daoType).list();
	}

}
