package hu.sol.esme.data.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import hu.sol.esme.data.dao.AspectDao;
import hu.sol.esme.data.model.Aspect;

@Repository
public class AspectDaoImpl extends GenericDaoImpl<Aspect, Serializable> implements AspectDao {

	private static final Logger LOG = LogManager.getLogger(AspectDaoImpl.class);

	@SuppressWarnings("unchecked")
	@Override
	public List<Aspect> listByName(String name) {
		Criteria criteria = currentSession().createCriteria(daoType);

		//@formatter:off
		criteria.add(Restrictions.like("name", name))
				.addOrder(Order.desc("name"));
		//@formatter:on

		return criteria.list();
	}
}
