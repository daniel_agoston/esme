package hu.sol.esme.data.dao.impl;

import java.io.Serializable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import hu.sol.esme.data.dao.RoomDao;
import hu.sol.esme.data.model.Room;

@Repository
public class RoomDaoImpl extends GenericDaoImpl<Room, Serializable> implements RoomDao {

	private static final Logger LOG = LogManager.getLogger(RoomDaoImpl.class);
}
