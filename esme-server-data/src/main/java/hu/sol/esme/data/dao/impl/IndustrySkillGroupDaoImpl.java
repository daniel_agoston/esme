package hu.sol.esme.data.dao.impl;

import java.io.Serializable;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.thoughtworks.xstream.XStream;

import hu.sol.esme.data.dao.IndustrySkillGroupDao;
import hu.sol.esme.data.model.IndustrySkillGroup;

@Repository
public class IndustrySkillGroupDaoImpl extends GenericDaoImpl<IndustrySkillGroup, Serializable>
		implements IndustrySkillGroupDao {

	private static final Logger LOG = LogManager.getLogger(TechnicalSkillGroupDaoImpl.class);

	@Override
	public IndustrySkillGroup loadIndustrySkillGroupByIdWithSkills(Long id) {
		currentSession().enableFetchProfile("loadTechSkillGroupWithSkills");
		LOG.error(id);
		IndustrySkillGroup industrySkillGroup = currentSession().get(IndustrySkillGroup.class, id);
		LOG.error(new XStream().toXML(industrySkillGroup));
		currentSession().disableFetchProfile("loadTechSkillGroupWithSkills");

		return industrySkillGroup;
	}

}
