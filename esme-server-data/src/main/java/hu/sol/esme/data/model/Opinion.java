package hu.sol.esme.data.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.FetchProfile;
import org.hibernate.annotations.FetchProfiles;
import org.hibernate.envers.Audited;
import org.springframework.stereotype.Component;

@FetchProfiles(value = { @FetchProfile(name = "loadAllOpinionData", fetchOverrides = {
		@FetchProfile.FetchOverride(association = "user", entity = Opinion.class, mode = FetchMode.JOIN),
		@FetchProfile.FetchOverride(association = "createdBy", entity = Opinion.class, mode = FetchMode.JOIN),
		@FetchProfile.FetchOverride(association = "comment", entity = Opinion.class, mode = FetchMode.JOIN) }) })
@Component
@Entity
@Audited
@Table(name = "opinion")
@Inheritance(strategy = InheritanceType.JOINED)
public class Opinion implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_from")
	private User user;

	@OneToOne(fetch = FetchType.LAZY)
	private Comment comment;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "created_by")
	private User createdBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Comment getComment() {
		return comment;
	}

	public void setComment(Comment comment) {
		this.comment = comment;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

}
