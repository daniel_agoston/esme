package hu.sol.esme.data.dao.impl;

import java.io.Serializable;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import hu.sol.esme.data.dao.TechnicalSkillDao;
import hu.sol.esme.data.model.TechnicalSkill;

@Repository
public class TechnicalSkillDaoImpl extends GenericDaoImpl<TechnicalSkill, Serializable> implements TechnicalSkillDao {

	private static final Logger LOG = LogManager.getLogger(TechnicalSkillDaoImpl.class);

	@Override
	public TechnicalSkill loadTechnicalSkillByIdWithGroup(Long id) {
		currentSession().enableFetchProfile("loadTechSkillWithGroup");
		TechnicalSkill technicalSkill = currentSession().get(TechnicalSkill.class, id);
		currentSession().disableFetchProfile("loadTechSkillWithGroup");
		return technicalSkill;
	}
}
