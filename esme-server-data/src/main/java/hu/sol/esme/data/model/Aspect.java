package hu.sol.esme.data.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;
import org.springframework.stereotype.Component;

@Component
@Entity
@Audited
@Table(name = "aspect")
public class Aspect implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "name")
	@Size(max = 50, message = "A szempont neve nem lehet több {max} karakternél.")
	private String name;

	@Column(name = "description")
	@Size(max = 1000, message = "A szempont leírása nem lehet több {max} karakternél.")
	private String description;

	@Column(name = "type")
	@Size(max = 150, message = "A szempont típusa nem lehet több {max} karakternél.")
	private String type;
}
