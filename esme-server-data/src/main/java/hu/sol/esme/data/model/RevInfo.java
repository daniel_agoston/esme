package hu.sol.esme.data.model;

import java.sql.Date;
import java.text.DateFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;
import org.springframework.stereotype.Component;

import hu.sol.esme.data.util.RevInfoListener;

@Component
@Entity
@Table(name = "REVINFO")
@RevisionEntity(RevInfoListener.class)
public class RevInfo {

	@Id
	@GeneratedValue
	@RevisionNumber
	private int id;

	@RevisionTimestamp
	private long timestamp;

	@Column(name = "USER_NAME")
	private String userName;

	@Column(name = "DATE_OPER")
	private Date dateOperation;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Transient
	public Date getRevisionDate() {
		return new Date(timestamp);
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof DefaultRevisionEntity))
			return false;

		DefaultRevisionEntity that = (DefaultRevisionEntity) o;

		if (id != that.getId())
			return false;
		if (timestamp != that.getTimestamp())
			return false;

		return true;
	}

	public int hashCode() {
		int result;
		result = id;
		result = 31 * result + (int) (timestamp ^ (timestamp >>> 32));
		return result;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getDateOperation() {
		return dateOperation;
	}

	public void setDateOperation(Date dateOperation) {
		this.dateOperation = dateOperation;
	}

	public String toString() {
		return "DefaultRevisionEntity(id = " + id + ", revisionDate = "
				+ DateFormat.getDateTimeInstance().format(getRevisionDate()) + ")";
	}
}
