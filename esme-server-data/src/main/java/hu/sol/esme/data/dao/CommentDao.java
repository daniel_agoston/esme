package hu.sol.esme.data.dao;

import java.io.Serializable;

import hu.sol.esme.data.model.Comment;

public interface CommentDao extends GenericDao<Comment, Serializable> {

}
