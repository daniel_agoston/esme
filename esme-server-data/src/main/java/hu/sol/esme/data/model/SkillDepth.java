package hu.sol.esme.data.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.envers.Audited;
import org.springframework.stereotype.Component;

@Component
@Entity
@Audited
@Table(name = "skill_depth",
	uniqueConstraints = @UniqueConstraint(columnNames = { "skill_id", "depth_id" }))
public class SkillDepth implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@OneToOne(fetch = FetchType.LAZY, optional = false)
	@Cascade({ CascadeType.MERGE })
	@JoinColumn(name = "skill_id", referencedColumnName = "id")
	private Skill skill;

	@OneToOne(fetch = FetchType.LAZY, optional = false)
	@Cascade({ CascadeType.MERGE })
	@JoinColumn(name = "depth_id", referencedColumnName = "id")
	private Depth depth;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Skill getSkill() {
		return skill;
	}

	public void setSkill(Skill skill) {
		this.skill = skill;
	}

	public Depth getDepth() {
		return depth;
	}

	public void setDepth(Depth depth) {
		this.depth = depth;
	}

}
