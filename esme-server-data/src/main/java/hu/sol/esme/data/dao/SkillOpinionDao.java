package hu.sol.esme.data.dao;

import java.io.Serializable;
import java.util.List;

import hu.sol.esme.data.model.SkillOpinion;

public interface SkillOpinionDao extends GenericDao<SkillOpinion, Serializable> {

	public List<SkillOpinion> listSkillOpinionByCreator(String creator);

	public List<SkillOpinion> listSkillOpinionByName(String name);

}
