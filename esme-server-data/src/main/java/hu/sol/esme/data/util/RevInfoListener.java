package hu.sol.esme.data.util;

import java.sql.Date;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.envers.RevisionListener;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import hu.sol.esme.data.model.RevInfo;

public class RevInfoListener implements RevisionListener {

	private static final Logger LOG = LogManager.getLogger(RevInfoListener.class);

	@Override
	public void newRevision(Object revisionEntity) {
		RevInfo revision = (RevInfo) revisionEntity;

		SecurityContext context = SecurityContextHolder.getContext();
		Authentication authentication = context.getAuthentication();
		String userName = "---";
		try {
			UserDetails userDetails = (UserDetails) authentication.getPrincipal();
			if (userDetails != null) {
				userName = userDetails.getUsername();
			} else {
				userName = "UNKNOWN";
			}
		} catch (Exception e) {
			LOG.warn("Cannot load user prinicipal from user details: " + e.getMessage());
			try {
				userName = (String) authentication.getPrincipal();
			} catch (Exception ex) {
				userName = "UNKNOWN";
				LOG.warn("Cannot load userName prinicipal: " + ex.getMessage());
			}
		}
		LOG.debug(userName);
		revision.setUserName(userName);
		revision.setDateOperation(new Date(revision.getTimestamp()));
	}
}
