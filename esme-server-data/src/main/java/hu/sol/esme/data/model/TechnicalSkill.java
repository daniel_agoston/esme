package hu.sol.esme.data.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.FetchProfile;
import org.hibernate.annotations.FetchProfiles;
import org.hibernate.envers.Audited;
import org.springframework.stereotype.Component;

@FetchProfiles(value = { @FetchProfile(name = "loadTechSkillWithGroup", fetchOverrides = {
		@FetchProfile.FetchOverride(association = "technicalSkillGroup", entity = TechnicalSkill.class, mode = FetchMode.JOIN) }) })
@Component
@Entity
@Audited
@Table(name = "techn_skill")
@PrimaryKeyJoinColumn(name = "skill_id", referencedColumnName = "id")
public class TechnicalSkill extends Skill implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade({ CascadeType.SAVE_UPDATE })
	private TechnicalSkillGroup technicalSkillGroup;

	public TechnicalSkillGroup getTechnicalSkillGroup() {
		return technicalSkillGroup;
	}

	public void setTechnicalSkillGroup(TechnicalSkillGroup technicalSkillGroup) {
		this.technicalSkillGroup = technicalSkillGroup;
	}

}
