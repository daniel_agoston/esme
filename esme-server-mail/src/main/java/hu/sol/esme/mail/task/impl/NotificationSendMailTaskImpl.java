package hu.sol.esme.mail.task.impl;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import hu.sol.esme.mail.task.NotificationSendMailTask;

@Transactional(propagation = Propagation.REQUIRED)
public class NotificationSendMailTaskImpl implements NotificationSendMailTask {

	private final static Logger logger = Logger.getLogger(NotificationSendMailTask.class);

	@Autowired
	private JavaMailSenderImpl mailSender;

	public NotificationSendMailTaskImpl() {
	}

	@PostConstruct
	public void postConstruct() {
	}

	public void sendMails() {
		logger.info("Mail init start");
		sendMail();
		logger.info("End Mails Send");
	}

	public void sendMail() {
		SimpleMailMessage simpleMailMessage = getSimpleMailMessage();
		MimeMessage message = mailSender.createMimeMessage();

		try {

			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(simpleMailMessage.getFrom());
			helper.setTo(simpleMailMessage.getTo());
			helper.setSubject(simpleMailMessage.getSubject());
			helper.setText(simpleMailMessage.getText());

		} catch (MessagingException e) {
			throw new MailParseException(e);
		}
		mailSender.send(message);
	}

	private SimpleMailMessage getSimpleMailMessage() {
		SimpleMailMessage smm = new SimpleMailMessage();
		setDummySimpleMailMessage(smm);
		return smm;
	}

	private void setDummySimpleMailMessage(SimpleMailMessage smm) {
		smm.setFrom("noreply@suitsolutions.eu");
		smm.setTo("daniel.agoston@suitsolutions.eu");
		smm.setSubject("Kormányablak elégedettség havi jelentés");
		smm.setText("A havi jelentés a csatolmányban található.");
	}
}
