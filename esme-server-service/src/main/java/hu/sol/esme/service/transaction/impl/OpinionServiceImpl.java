package hu.sol.esme.service.transaction.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import hu.sol.esme.data.dao.AspectOpinionDao;
import hu.sol.esme.data.dao.SkillOpinionDao;
import hu.sol.esme.data.model.AspectOpinion;
import hu.sol.esme.data.model.SkillOpinion;
import hu.sol.esme.service.transaction.OpinionService;

@Service
public class OpinionServiceImpl implements OpinionService {

	private static final Logger LOG = LogManager.getLogger(OpinionServiceImpl.class);

	@Autowired
	private AspectOpinionDao aspectOpinionDao;

	@Autowired
	private SkillOpinionDao skillOpinionDao;

	@Override
	@Transactional(readOnly = true)
	public SkillOpinion getSkillOpinionById(Long id) {
		return skillOpinionDao.find(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public SkillOpinion saveSkillOpinion(SkillOpinion opinion) {
		return skillOpinionDao.merge(opinion);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public SkillOpinion updateSkillOpinion(SkillOpinion opinion) {
		return skillOpinionDao.merge(opinion);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteSkillOpinion(SkillOpinion opinion) {
		skillOpinionDao.remove(opinion);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteSkillOpinionById(Long id) {
		SkillOpinion opinion = skillOpinionDao.find(id);
		skillOpinionDao.remove(opinion);
	}

	@Override
	@Transactional(readOnly = true)
	public List<SkillOpinion> listSkillOpinion() {
		return skillOpinionDao.listAll();
	}

	@Override
	@Transactional(readOnly = true)
	public List<SkillOpinion> listSkillOpinionByCreator(String creator) {
		return skillOpinionDao.listSkillOpinionByCreator(creator);
	}

	@Override
	@Transactional(readOnly = true)
	public List<SkillOpinion> listSkillOpinionByUsername(String username) {
		return skillOpinionDao.listSkillOpinionByName(username);
	}

	@Override
	@Transactional(readOnly = true)
	public AspectOpinion getAspectOpinionById(Long id) {
		return aspectOpinionDao.find(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public AspectOpinion saveAspectOpinion(AspectOpinion opinion) {
		return aspectOpinionDao.merge(opinion);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public AspectOpinion updateAspectOpinion(AspectOpinion opinion) {
		return aspectOpinionDao.merge(opinion);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteAspectOpinion(AspectOpinion opinion) {
		aspectOpinionDao.remove(opinion);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteAspectOpinionById(Long id) {
		AspectOpinion opinion = aspectOpinionDao.find(id);
		aspectOpinionDao.remove(opinion);

	}

	@Override
	@Transactional(readOnly = true)
	public List<AspectOpinion> listAspectOpinion() {
		return aspectOpinionDao.listAll();
	}

	@Override
	@Transactional(readOnly = true)
	public List<AspectOpinion> listAspectOpinionByCreator(String creator) {
		return aspectOpinionDao.listAspectOpinionByCreator(creator);
	}

	@Override
	@Transactional(readOnly = true)
	public List<AspectOpinion> listAspectOpinionByUsername(String username) {
		return aspectOpinionDao.listAspectOpinionByName(username);
	}

}
