package hu.sol.esme.service.transaction;

import java.util.List;

import hu.sol.esme.data.model.Depth;

public interface DepthService {

	public Depth getDepthById(Long id);

	public List<Depth> listDepthByGroup(String group);

	public List<Depth> listDepth();

	public Depth saveDepth(Depth depth);

	public Depth updateDepth(Depth depth);

	public void deleteDepth(Depth depth);

}
