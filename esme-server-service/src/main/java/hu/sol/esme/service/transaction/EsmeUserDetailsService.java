package hu.sol.esme.service.transaction;

import org.springframework.security.core.userdetails.UserDetailsService;

import hu.sol.esme.data.model.User;

public interface EsmeUserDetailsService extends UserDetailsService {

	public void registerUser(User user);

	public void updateUser(User user);

}
