package hu.sol.esme.service.transaction.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import hu.sol.esme.data.dao.RoomDao;
import hu.sol.esme.data.model.Room;
import hu.sol.esme.service.transaction.RoomService;

@Service
public class RoomServiceImpl implements RoomService {

	private static final Logger LOG = LogManager.getLogger(RoomServiceImpl.class);

	@Autowired
	private RoomDao roomDao;

	@Override
	@Transactional(readOnly = true)
	public Room getRoomById(Long id) {
		return roomDao.find(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Room saveRoom(Room room) {
		return roomDao.merge(room);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Room updateRoom(Room room) {
		return roomDao.merge(room);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteRoom(Room room) {
		roomDao.remove(room);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Room updateRoomOpeningTime(Room room) {
		Room savedRoom = roomDao.find(room.getId());

		savedRoom.setOpenFrom(room.getOpenFrom());
		savedRoom.setOpenTo(room.getOpenTo());

		return roomDao.merge(savedRoom);
	}

}
