package hu.sol.esme.service.transaction.impl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import hu.sol.esme.data.dao.IndustrySkillGroupDao;
import hu.sol.esme.data.dao.TechnicalSkillGroupDao;
import hu.sol.esme.data.model.IndustrySkillGroup;
import hu.sol.esme.data.model.TechnicalSkillGroup;
import hu.sol.esme.service.transaction.SkillGroupService;

@Service
public class SkillGroupServiceImpl implements SkillGroupService {

	private static final Logger LOG = LogManager.getLogger(SkillGroupServiceImpl.class);

	@Autowired
	private TechnicalSkillGroupDao technicalSkillGroupDao;

	@Autowired
	private IndustrySkillGroupDao industrySkillGroupDao;

	@PostConstruct
	private void initSkillGroupService() {
		LOG.debug("init SkillGroupService");
	}

	@Override
	@Transactional(readOnly = true)
	public TechnicalSkillGroup getTechnicalSkillGroupByIdWithSkills(Long id) {
		LOG.debug("getTechnicalSkillGroupByIdWithSkills called.");
		TechnicalSkillGroup result = technicalSkillGroupDao.loadTechnicalSkillGroupByIdWithSkills(id);
		return result;
	}

	@Override
	@Transactional(readOnly = true)
	public TechnicalSkillGroup getTechnicalSkillGroupById(Long id) {
		LOG.debug("getTechnicalSkillGroupById called.");
		TechnicalSkillGroup result = technicalSkillGroupDao.find(id);
		return result;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public TechnicalSkillGroup saveTechnicalSkillGroup(TechnicalSkillGroup technicalSkillGroup) {
		return technicalSkillGroupDao.merge(technicalSkillGroup);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public TechnicalSkillGroup updateTechnicalSkillGroup(TechnicalSkillGroup technicalSkillGroup) {
		technicalSkillGroupDao.update(technicalSkillGroup);
		return technicalSkillGroupDao.find(technicalSkillGroup.getId());
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteTechnicalSkillGroup(TechnicalSkillGroup technicalSkillGroup) {
		technicalSkillGroupDao.remove(technicalSkillGroup);
	}

	@Override
	@Transactional(readOnly = true)
	public IndustrySkillGroup getIndustrySkillGroupByIdWithSkills(Long id) {
		LOG.debug("getTechnicalSkillGroupByIdWithSkills called.");
		IndustrySkillGroup result = industrySkillGroupDao.loadIndustrySkillGroupByIdWithSkills(id);
		return result;
	}

	@Override
	@Transactional(readOnly = true)
	public IndustrySkillGroup getIndustrySkillGroupById(Long id) {
		LOG.debug("getIndustrySkillGroupById called.");
		IndustrySkillGroup result = industrySkillGroupDao.find(id);
		return result;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public IndustrySkillGroup saveIndustrySkillGroup(IndustrySkillGroup industrySkillGroup) {
		return industrySkillGroupDao.merge(industrySkillGroup);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public IndustrySkillGroup updateIndustrySkillGroup(IndustrySkillGroup industrySkillGroup) {
		industrySkillGroupDao.update(industrySkillGroup);
		return industrySkillGroupDao.find(industrySkillGroup.getId());
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteIndustrySkillGroup(IndustrySkillGroup industrySkillGroup) {
		industrySkillGroupDao.remove(industrySkillGroup);
	}

	@Override
	@Transactional(readOnly = true)
	public List<TechnicalSkillGroup> listTechnicalSkillGroup() {
		return technicalSkillGroupDao.listAll();
	}

	@Override
	@Transactional(readOnly = true)
	public List<IndustrySkillGroup> listIndustrySkillGroup() {
		return industrySkillGroupDao.listAll();
	}
}
