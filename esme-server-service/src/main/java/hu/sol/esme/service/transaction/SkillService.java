package hu.sol.esme.service.transaction;

import hu.sol.esme.data.model.IndustrySkill;
import hu.sol.esme.data.model.TechnicalSkill;

public interface SkillService {

	public TechnicalSkill getTechnicalSkillById(Long id);

	public TechnicalSkill getTechnicalSkillByIdWithGroup(Long id);

	public TechnicalSkill saveTechnicalSkill(TechnicalSkill technicalSkill);

	public IndustrySkill getIndustrySkillById(Long id);

	public IndustrySkill getIndustrySkillByIdWithGroup(Long id);

	public IndustrySkill saveIndustrySkill(IndustrySkill industrySkill);
}
