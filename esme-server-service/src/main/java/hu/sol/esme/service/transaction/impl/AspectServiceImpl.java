package hu.sol.esme.service.transaction.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import hu.sol.esme.data.dao.AspectDao;
import hu.sol.esme.data.model.Aspect;
import hu.sol.esme.service.transaction.AspectService;

@Service
public class AspectServiceImpl implements AspectService {

	private static final Logger LOG = LogManager.getLogger(AspectServiceImpl.class);

	@Autowired
	private AspectDao aspectDao;

	@Override
	@Transactional(readOnly = true)
	public Aspect getAspectById(Long id) {
		return aspectDao.find(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Aspect saveAspect(Aspect aspect) {
		return aspectDao.merge(aspect);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Aspect updateAspect(Aspect aspect) {
		return aspectDao.merge(aspect);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteAspect(Aspect aspect) {
		aspectDao.remove(aspect);
	}

	@Override
	public void deleteAspectById(Long id) {
		Aspect aspect = aspectDao.find(id);
		aspectDao.remove(aspect);

	}

	@Override
	@Transactional(readOnly = true)
	public List<Aspect> listAspect() {
		return aspectDao.listAll();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Aspect> listAspectByName(String name) {
		return aspectDao.listByName(name);
	}

}
