package hu.sol.esme.service.transaction;

import java.util.List;

import hu.sol.esme.data.model.Project;

public interface ProjectService {

	public Project getProjectById(Long id);

	public List<Project> listProject();

	public void saveProject(Project project);

	public Project updateProject(Project project);

	public void deleteProject(Project project);

	public void deleteProjectById(Long id);

}
