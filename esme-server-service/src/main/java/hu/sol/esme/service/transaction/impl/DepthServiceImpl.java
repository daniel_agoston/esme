package hu.sol.esme.service.transaction.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import hu.sol.esme.data.dao.DepthDao;
import hu.sol.esme.data.model.Depth;
import hu.sol.esme.service.transaction.DepthService;

public class DepthServiceImpl implements DepthService {

	private static final Logger LOG = LogManager.getLogger(DepthServiceImpl.class);

	@Autowired
	private DepthDao depthDao;

	@Override
	@Transactional(readOnly = true)
	public Depth getDepthById(Long id) {
		return depthDao.find(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Depth> listDepthByGroup(String group) {
		return depthDao.listDepthByGroup(group);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Depth> listDepth() {
		return depthDao.listAll();
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Depth saveDepth(Depth depth) {
		return depthDao.merge(depth);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Depth updateDepth(Depth depth) {
		return depthDao.merge(depth);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteDepth(Depth depth) {
		depthDao.remove(depth);
	}
}
