package hu.sol.esme.service.transaction.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import hu.sol.esme.data.dao.UserDao;
import hu.sol.esme.data.model.User;
import hu.sol.esme.service.transaction.EsmeUserDetailsService;

@Service(value = "esmeUserDetailsService")
public class EsmeUserDetailsServiceImpl implements EsmeUserDetailsService {

	private static final Logger LOG = LogManager.getLogger(EsmeUserDetailsServiceImpl.class);

	@Autowired
	private UserDao userDao;

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) {

		LOG.debug("UserName: " + username);
		User user = null;
		user = userDao.findByUserName(username);

		// check dao response..
		if (user == null) {
			LOG.debug("Debug point1: User not found..");
			throw new UsernameNotFoundException("username " + username + " not found");
		}
		if (StringUtils.isEmpty(user.getUsername())) {
			LOG.debug("Debug point2: User not found..");
			throw new UsernameNotFoundException("Username not found..");
		}
		if (StringUtils.isEmpty(user.getEmail())) {
			LOG.debug("Debug point3: User not found..");
			throw new UsernameNotFoundException("Email not found..");
		}

		LOG.debug("Debug point4: User found..");
		// if everything is ok..
		return user;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void registerUser(User user) {
		userDao.registerUser(user);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void updateUser(User user) {
		userDao.updateUser(user);
	}

}
