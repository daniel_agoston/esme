package hu.sol.esme.service.transaction.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import hu.sol.esme.data.dao.ProjectDao;
import hu.sol.esme.data.model.Project;
import hu.sol.esme.service.transaction.ProjectService;

@Service
public class ProjectServiceImpl implements ProjectService {

	private static final Logger LOG = LogManager.getLogger(ProjectServiceImpl.class);

	@Autowired
	private ProjectDao projectDao;

	@Override
	@Transactional(readOnly = true)
	public Project getProjectById(Long id) {
		LOG.debug("getProjectById called");
		Project project = projectDao.find(id);
		return project;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Project> listProject() {
		LOG.debug("listProject called");
		return projectDao.listAll();
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveProject(Project project) {
		projectDao.merge(project);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Project updateProject(Project project) {
		Project result = projectDao.merge(project);
		return result;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteProject(Project project) {
		projectDao.remove(project);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteProjectById(Long id) {
		Project project = projectDao.find(id);
		projectDao.remove(project);
	}

}
