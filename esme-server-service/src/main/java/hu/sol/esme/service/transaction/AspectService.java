package hu.sol.esme.service.transaction;

import java.util.List;

import hu.sol.esme.data.model.Aspect;

public interface AspectService {

	public Aspect getAspectById(Long id);

	public Aspect saveAspect(Aspect aspect);

	public Aspect updateAspect(Aspect aspect);

	public void deleteAspect(Aspect aspect);

	public void deleteAspectById(Long id);

	public List<Aspect> listAspect();

	public List<Aspect> listAspectByName(String name);
}
