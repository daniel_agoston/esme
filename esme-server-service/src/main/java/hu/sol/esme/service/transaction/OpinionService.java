package hu.sol.esme.service.transaction;

import java.util.List;

import hu.sol.esme.data.model.AspectOpinion;
import hu.sol.esme.data.model.SkillOpinion;

public interface OpinionService {

	public SkillOpinion getSkillOpinionById(Long id);

	public SkillOpinion saveSkillOpinion(SkillOpinion opinion);

	public SkillOpinion updateSkillOpinion(SkillOpinion opinion);

	public void deleteSkillOpinion(SkillOpinion opinion);

	public void deleteSkillOpinionById(Long id);

	public List<SkillOpinion> listSkillOpinion();

	public List<SkillOpinion> listSkillOpinionByCreator(String creator);

	public List<SkillOpinion> listSkillOpinionByUsername(String username);

	public AspectOpinion getAspectOpinionById(Long id);

	public AspectOpinion saveAspectOpinion(AspectOpinion opinion);

	public AspectOpinion updateAspectOpinion(AspectOpinion opinion);

	public void deleteAspectOpinion(AspectOpinion opinion);

	public void deleteAspectOpinionById(Long id);

	public List<AspectOpinion> listAspectOpinion();

	public List<AspectOpinion> listAspectOpinionByCreator(String creator);

	public List<AspectOpinion> listAspectOpinionByUsername(String username);

}
