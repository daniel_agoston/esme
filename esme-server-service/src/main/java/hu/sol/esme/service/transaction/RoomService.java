package hu.sol.esme.service.transaction;

import hu.sol.esme.data.model.Room;

public interface RoomService {

	public Room getRoomById(Long id);

	public Room saveRoom(Room room);

	public Room updateRoom(Room room);

	public void deleteRoom(Room room);

	public Room updateRoomOpeningTime(Room room);
}
