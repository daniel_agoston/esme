package hu.sol.esme.service.transaction.impl;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import hu.sol.esme.data.dao.IndustrySkillDao;
import hu.sol.esme.data.dao.TechnicalSkillDao;
import hu.sol.esme.data.model.IndustrySkill;
import hu.sol.esme.data.model.TechnicalSkill;
import hu.sol.esme.service.transaction.SkillService;

@Service
public class SkillServiceImpl implements SkillService {

	private static final Logger LOG = LogManager.getLogger(SkillServiceImpl.class);

	@Autowired
	private TechnicalSkillDao technicalSkillDao;

	@Autowired
	private IndustrySkillDao industrySkillDao;

	@PostConstruct
	private void initSkillService() {
		LOG.debug("init Skill Service");
	}

	@Override
	@Transactional(readOnly = true)
	public TechnicalSkill getTechnicalSkillById(Long id) {
		LOG.debug("getTechnicalSkillById called.");
		return technicalSkillDao.find(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public TechnicalSkill saveTechnicalSkill(TechnicalSkill technicalSkill) {
		return technicalSkillDao.merge(technicalSkill);
	}

	@Override
	@Transactional(readOnly = true)
	public IndustrySkill getIndustrySkillById(Long id) {
		LOG.debug("getIndustrySkillById called.");
		IndustrySkill result = industrySkillDao.find(id);
		return result;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public IndustrySkill saveIndustrySkill(IndustrySkill industrySkill) {
		return industrySkillDao.merge(industrySkill);
	}

	@Override
	@Transactional(readOnly = true)
	public TechnicalSkill getTechnicalSkillByIdWithGroup(Long id) {
		return technicalSkillDao.loadTechnicalSkillByIdWithGroup(id);
	}

	@Override
	@Transactional(readOnly = true)
	public IndustrySkill getIndustrySkillByIdWithGroup(Long id) {
		return industrySkillDao.loadIndustrySkillByIdWithGroup(id);
	}

}
