package hu.sol.esme.service.transaction;

import java.util.List;

import hu.sol.esme.data.model.IndustrySkillGroup;
import hu.sol.esme.data.model.TechnicalSkillGroup;

public interface SkillGroupService {

	public TechnicalSkillGroup getTechnicalSkillGroupByIdWithSkills(Long id);

	public TechnicalSkillGroup getTechnicalSkillGroupById(Long id);

	public List<TechnicalSkillGroup> listTechnicalSkillGroup();

	public TechnicalSkillGroup saveTechnicalSkillGroup(TechnicalSkillGroup technicalSkillGroup);

	public TechnicalSkillGroup updateTechnicalSkillGroup(TechnicalSkillGroup technicalSkillGroup);

	public void deleteTechnicalSkillGroup(TechnicalSkillGroup technicalSkillGroup);

	public IndustrySkillGroup getIndustrySkillGroupByIdWithSkills(Long id);

	public IndustrySkillGroup getIndustrySkillGroupById(Long id);

	public List<IndustrySkillGroup> listIndustrySkillGroup();

	public IndustrySkillGroup saveIndustrySkillGroup(IndustrySkillGroup industrySkillGroup);

	public IndustrySkillGroup updateIndustrySkillGroup(IndustrySkillGroup industrySkillGroup);

	public void deleteIndustrySkillGroup(IndustrySkillGroup industrySkillGroup);
}
