package hu.sol.esme.rest;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import hu.sol.esme.service.transaction.impl.SkillGroupServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-datasource-config.xml",
		"classpath:spring/test-rest-config.xml" })
public class TestTechnicalSkillGroupService {

	private static final Logger LOG = LogManager.getLogger(TestTechnicalSkillService.class);

	@Autowired
	private SkillGroupServiceImpl SkillGroupRestService;
}
